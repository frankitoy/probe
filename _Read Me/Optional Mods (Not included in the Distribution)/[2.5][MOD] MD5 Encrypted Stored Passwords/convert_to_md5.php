<?php
/**
 * This script changes all common (clear) passwords to an md5-hash.
 * First do all steps to get your CS-Version ready to use md5-hash saved passwords.
 * 
 * !!!Backup your database!!!
 * 
 * Copy this script into your main CS folder (e.g. /srv/www/htdocs/clipshare/clear_to_md5.php)
 */


/**
 *  Config part 
 * 
 *  Remind: much screen output will slower everything.
 */

$display = 2; // 0 nothing (recommended) | 1 update querys | 2 uid & md5-password | 3 all


/* ############ DO NOT EDIT PART ################### */
include("include/config.php");
include("include/function.php");

/* Get all users and their passwords */
$SQL ="SELECT UID, pwd FROM signup";
$RESULT = $conn->Execute($SQL);

$mysql_error = '0';

if (mysql_error()) {
	echo 'There was an mysql error:<br />'.mysql_error();
	die;
}

/* Hits you got from database */
$hits = $RESULT->recordcount();

/* Array with information about the user (UID, pwd) */
$users = $RESULT->getrows();

/* Updates the passwords in a foreach loop. */
foreach ($users as $array) {
	$new_pwd = md5($array['pwd']);
	
	$SQL ='UPDATE signup SET pwd = \''.$new_pwd.'\' WHERE UID = \''.$array['UID'].'\'';
	$RESULT = $conn->Execute($SQL);
	
	/* Count mysql errors */
	if (mysql_error()) {
		$mysql_error = $mysql_error + 1;
	}
	
	/* display the screen output */
	switch ($display) {
		case '1':
				echo $SQL.'<br />';
			break;
		case '2':
				echo 'UID: '.$array['UID'].' | Password MD5-Hash: '.$new_pwd.'<br />';
			break;
		case '3':
				echo 'UID: '.$array['UID'].' | Password MD5-Hash: '.$new_pwd.'<br />SQL: '.$SQL.'<br /><hr /><br />';
		default:
			break;
	}
}	

echo '<br /><br />';
echo '<hr />';
echo 'Stats:<br />';
echo 'Users Updated: '.$hits.'<br />';
echo 'MySQL Errors: '.$mysql_error.'<br />';
echo '<hr />';
?>