<?php /* Smarty version 2.6.6, created on 2009-11-08 11:33:45
         compiled from outbox.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'outbox.tpl', 52, false),array('modifier', 'date_format', 'outbox.tpl', 56, false),)), $this); ?>
<br>

<TABLE cellSpacing=0 cellPadding=5 width="100%" border=0>
<TBODY>
<tr>
        <TD vAlign=top align="center"><!--Begin Gray Table-->
        <TABLE cellSpacing=0 cellPadding=0 width=580 align=center bgColor=#f5f5f5 border=0>
        <TBODY>
        <TR>
        <TD>

                <DIV>
                <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
                <TBODY>
                 <tr>
                          <td align="center" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td align="center">
                                                                <table width="101%"  border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td width="5" height="24" background="images/nv_2_l.jpg"><img src="images/nv_2_l.jpg" width="5" height="24" alt=""></td>
                                  <th background="images/nv_2_bg.jpg"><div align="left">
                                      <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td class="white_bold">Messages//Outbox</td>
                                          <td width="150" class="white_regular" align=right style="padding-right:10px;"><?php if ($this->_tpl_vars['total'] != '0'): ?>Messages <?php echo $this->_tpl_vars['start_num']; ?>
 - <?php echo $this->_tpl_vars['end_num']; ?>
 of <?php echo $this->_tpl_vars['total'];  endif; ?> </td>
                                          </tr>
                                      </table>
                                  </div></th>
                                  <td width="5" background="images/nv_2_r.jpg"><img src="images/nv_2_r.jpg" width="5" height="24" alt=""></td>
                                </tr>
                              </table></td>
                            </tr>


                </TBODY>
                </TABLE>
                </DIV>

                <table width = 600 cellpadding=3 cellspacing=0 table="table" align=center>
                <tr><td colspan="5" height="10"></td></tr>
                <?php if ($this->_tpl_vars['total'] != '0'): ?>
                <tr>
                        <td width=20>&nbsp;</td>
                        <td><b>Subject</b></td>
                        <td width=70><b>To<b></td>
                        <td width=160><b>Date</b></td>
                        <td width=20>&nbsp;</td>
                </tr>
                <?php endif; ?>
                <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['pm_id']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                <tr bgcolor="<?php echo smarty_function_cycle(array('values' => "#eeeeee,#f5f5f5"), $this);?>
">
                        <td width=5><img src='<?php echo $this->_tpl_vars['baseurl']; ?>
/images/mail.gif'></td>
                        <td><a href="<?php echo $this->_tpl_vars['baseurl']; ?>
/msg.php?id=<?php echo $this->_tpl_vars['pm_id'][$this->_sections['i']['index']]; ?>
"><?php echo $this->_tpl_vars['subject'][$this->_sections['i']['index']]; ?>
</a></td>
                        <td><?php echo $this->_tpl_vars['receiver'][$this->_sections['i']['index']]; ?>
</td>
                        <td><?php echo ((is_array($_tmp=$this->_tpl_vars['date'][$this->_sections['i']['index']])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%A, %B %e, %Y") : smarty_modifier_date_format($_tmp, "%A, %B %e, %Y")); ?>

                        <td width=10><a href="<?php echo $this->_tpl_vars['baseurl']; ?>
/outbox.php?id=<?php echo $this->_tpl_vars['pm_id'][$this->_sections['i']['index']]; ?>
&action=del" onclick="javascript:return confirm('Are you sure you want to delete this message?');">Delete</a></td>
                </td>
                </tr>
                <?php endfor; endif; ?>
                <?php if ($this->_tpl_vars['pm_id'] == ""): ?>
                <tr class=tablerow>
                        <td align=center colspan=5><br>You have no messages in your outbox<br><br></td>
                </tr>
                <?php endif; ?>
                </table>

                <DIV>
                <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
                <TBODY>
                <TR vAlign=top>
                <TD><br>
                <?php if ($this->_tpl_vars['total'] != '0'): ?>
                        <DIV class=moduleTitle>Pages: <?php echo $this->_tpl_vars['page_link']; ?>
</DIV></TD>
                <?php endif; ?>
                </TR>
                </TBODY>
                </TABLE>
                </DIV>


                <!-- begin paging --><!-- end paging -->
        </TD>
        </TR>
        </TBODY>
        </TABLE>
        <!--End Gray Table--></TD><!--End Rigth Side Group List Table-->
</TR></TBODY></TABLE>

</td></tr>
</table>

<br>


