<?php

/**************************************************************************************************

| Software Name        : ClipShare - Video Sharing Community Script

| Software Author      : Clip-Share.Com / ScriptXperts.Com

| Website              : http://www.clip-share.com

| E-mail               : office@clip-share.com

|**************************************************************************************************

| This source file is subject to the ClipShare End-User License Agreement, available online at:

| http://www.clip-share.com/video-sharing-script-eula.html

| By using this software, you acknowledge having read this Agreement and agree to be bound thereby.

|**************************************************************************************************

| Copyright (c) 2006-2007 Clip-Share.com. All rights reserved.

|**************************************************************************************************/



session_start();

include("include/config.php");

include("include/function.php");

if ($config['approve'] == 1) {$active = "and active = '1'";}

# Find the vote information 

$mydate= date('Y-m-d');

$sql ="select * from poll_question  where start_date<='$mydate' and end_date>='$mydate'";

$rs = $conn->Execute($sql);

$x=$rs->getarray();

$list=explode("|", $x[0]['poll_answer']);

STemplate::assign('poll_id',$x[0]['poll_id']);

STemplate::assign('poll_qty',$x[0]['poll_qty']);

STemplate::assign('list',$list);

//stats start 
				if ($config['approve'] == 1) {$active = "and active = '1'";}

                $sql = "SELECT count(*) as total from video where type='public' $active"; 
                $ars = $conn->Execute($sql); 
                $total = $ars->fields['total']; 
                $grandtotalpublic = $total; 
                STemplate::assign('grandtotalpublic',$grandtotalpublic+0); 

                $sql = "SELECT count(*) as total from video where type='private' $active"; 
                $ars = $conn->Execute($sql); 
                $total = $ars->fields['total']; 
                $grandtotalprivate = $total; 
                STemplate::assign('grandtotalprivate',$grandtotalprivate+0); 

                $sql = "SELECT count(*) as total from signup $query"; 
                $ars = $conn->Execute($sql); 
                $total = $ars->fields['total']; 
                $grandtotaluser = $total; 
                STemplate::assign('grandtotaluser',$grandtotaluser+0); 


				$sql = "SELECT sum(viewnumber) as total from video";
				$ars = $conn->Execute($sql);
				STemplate::assign('vviews', number_format($ars->fields['total'],0,",",".")); 
//stats end

		//POPULAR CLOUD TAGS

		$featuredtagsquery="SELECT keyword from video where type='public' $active order by viewnumber desc limit 20";
		$tagshome=cloudTags($featuredtagsquery);
		STemplate::assign('tagshome',$tagshome);

		//END CLOUD TAGS

//PAGING

$items_per_page=($config[rows_per_page]*$config[cols_per_page]);

$sql="SELECT * from video where type='public' and featured='yes' $active order by addtime desc";

$rs = $conn->Execute($sql);

if($rs->recordcount()>0)$users = $rs->getrows();

//END PAGING



$sql1 = "select VID, title, viewtime, vkey from video where viewtime<>'0000-00-00 00:00:00' and type='public' $active order by viewtime desc limit 0, ".$config['recently_viewed_video']; 

$rs_v = $conn->execute($sql1);

$recent = $rs_v->getrows();

STemplate::assign('recent', $recent);

STemplate::assign('recent_total', count($recent));



if ($_REQUEST[msg]!=""){

	$msg=$_REQUEST[msg];

}



STemplate::assign('err',$err);

STemplate::assign('msg',$msg);

STemplate::assign('answers',$users);

STemplate::assign('total',$rs->recordcount());


STemplate::assign('head_bottom',"homelinks.tpl");



if($config['enable_package']=="yes" and $_SESSION[UID]!="")

{

        $sql = "select * from subscriber where UID=$_SESSION[UID]";

        $rs = $conn->execute($sql);

        $u_info = $rs->getrows();

        STemplate::assign('u_info', $u_info[0]);

        $rs->movefirst();

        

        $sql = "select * from package where pack_id=".$rs->fields['pack_id'];

        $rs = $conn->execute($sql);

        $pack = $rs->getrows();

        STemplate::assign('pack', $pack[0]);

}



STemplate::display('head1.tpl');

STemplate::display('err_msg.tpl');

STemplate::display('search.tpl');

STemplate::display('index.tpl');

STemplate::display('right.tpl');

STemplate::display('footer.tpl');

?>

