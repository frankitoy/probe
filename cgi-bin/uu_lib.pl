#******************************************************************************************************
#   ATTENTION: THIS FILE HEADER MUST REMAIN INTACT. DO NOT DELETE OR MODIFY THIS FILE HEADER.
#
#   Name: uu_lib.pl
#   Link: http://uber-uploader.sourceforge.net/
#   Revision: 1.0
#   Date: 2006/12/03
#   Author: Peter Schmandra
#   Description: Lib file
#
#   Licence:
#   The contents of this file are subject to the Mozilla Public
#   License Version 1.1 (the "License"); you may not use this file
#   except in compliance with the License. You may obtain a copy of
#   the License at http://www.mozilla.org/MPL/
# 
#   Software distributed under the License is distributed on an "AS
#   IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
#   implied. See the License for the specific language governing
#   rights and limitations under the License.
#
#*********************************************************************************************************

$DEBUG_ENABLED = 0;          # WARNING! Make sure to set DEBUG_ENABLED back to 0 when you go live.
$MULTI_CONFIGS_ENABLED = 0;  # Set this value to 1 if you are using multiple config files

#########################################
# Delete a directory and everthing in it
#########################################
sub deldir{
	my $del_dir = shift;
	
	if(-d $del_dir){
		if(opendir(DIRHANDLE, $del_dir)){
			my @file_list = readdir(DIRHANDLE);
			
			closedir(DIRHANDLE);
			
			foreach my $file (@file_list){
				unless( ($file eq ".") || ($file eq "..") ){ unlink($del_dir . "/" . $file); }
			}
			
			for(my $i = 0; $i < 5; $i++){
				if(rmdir($del_dir)){ last; }
				else{ sleep(1); }
			}
		}
		else{ warn("Cannont open $del_dir: $!"); }
	}
}

########################################################################
# Output a message to the screen 
#
# You can use this function to debug your script. 
#
# eg. &kak("The value of blarg is: " . $blarg . "<br>", 1, __LINE__);
# This will print the value of blarg and exit the script.
#
# eg. &kak("The value of blarg is: " . $blarg . "<br>", 0, __LINE__);
# This will print the value of blarg and continue the script.
########################################################################
sub kak{
	my $msg = shift;
	my $kak_exit = shift;
	my $line  = shift;
	
	if(!$print_issued){ 
		print "Content-type: text/html\n\n";
		$print_issued = 1; 
	}
	
	print "<!DOCTYPE HTML PUBLIC \"-\/\/W3C\/\/DTD HTML 4.01 Transitional\/\/EN\">\n";
	print "<html>\n";
	print "  <head>\n";
	print "    <title>UBER UPLOADER<\/title>\n";
	print "      <meta name=\"robots\" content=\"none\">\n";
	print "      <meta http-equiv=\"Pragma\" content=\"no-cache\">\n";
	print "      <meta http-equiv=\"CACHE-CONTROL\" content=\"no-cache\">\n";
	print "      <meta http-equiv=\"expires\" content=\"-1\">\n";
	print "  <\/head>\n";
	print "  <body style=\"background-color: #EEEEEE; color: #000000; font-family: arial, helvetica, sans_serif;\">\n";
	print "    <br>\n";
	print "    <div align='center'>\n";
	print "    $msg\n";
	print "    <br>\n";
	print "    <!-- uber_uploader.pl:kak on line $line -->\n";
	print "    </div>\n";
	print "  </body>\n";
	print "</html>\n";
	
	if($kak_exit){ exit; }
}

1;
