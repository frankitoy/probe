<?php
/**************************************************************************************************
| Software Name        : ClipShare - Video Sharing Community Script
| Software Author      : Clip-Share.Com / ScriptXperts.Com
| Website              : http://www.clip-share.com
| E-mail               : office@clip-share.com
|**************************************************************************************************
| This source file is subject to the ClipShare End-User License Agreement, available online at:
| http://www.clip-share.com/video-sharing-script-eula.html
| By using this software, you acknowledge having read this Agreement and agree to be bound thereby.
|**************************************************************************************************
| Copyright (c) 2006-2007 Clip-Share.com. All rights reserved.
|**************************************************************************************************/

session_start();

include("include/config.php");
include("include/function.php");
if ($config['approve'] == 1) {$active = "and active = '1'";}


if ( $_REQUEST[subscribe]== 'on' )
{
        if ( $_REQUEST[info] == 'i' )
        {
              $sql = " INSERT into subscribe_video set subscribe_to ='$_REQUEST[UID]', subscribe_from ='$_SESSION[UID]',status='on' ";
              $conn->execute( $sql );
        }
        else
        {
              $sql = " UPDATE subscribe_video set status='on' where subscribe_to ='$_REQUEST[UID]'and subscribe_from ='$_SESSION[UID]'";
              $conn->execute( $sql );
        }
}
else if ( $_REQUEST[subscribe]== 'off')
{
        $sql = " UPDATE subscribe_video set status='off' where subscribe_to ='$_REQUEST[UID]'and subscribe_from ='$_SESSION[UID]'";
        $conn->execute( $sql );
}





$sql="select * from signup WHERE UID='$_REQUEST[UID]'";
$rs=$conn->execute($sql);



if($rs->recordcount()>0)
{
        //INCREASE VIEW COUNT
        $sql="update signup set profile_viewed=profile_viewed+1 WHERE UID=$_REQUEST[UID]";
        $conn->execute($sql);
        
        //END
        $dat=explode("-",$rs->fields[bdate]);
        $age=(date("Y")-$dat[0]);
        STemplate::assign('profileage',$age);
        $users = $rs->getrows();
        $sql="select * from video WHERE UID=$_REQUEST[UID] $active order by VID desc limit 1";
        $rs=$conn->execute($sql);
        $vdo = $rs->getrows();
}
$chkuserflag="";

if(checklogin()){
        $chkuserflag="guest";
}

if ($_SESSION[myUID]==$_REQUEST[UID]){
                $chkuserflag="self";

}


STemplate::assign('UID',$_REQUEST[UID]);
STemplate::assign('chkuserflag',$chkuserflag);        
STemplate::assign('err',$err);
STemplate::assign('msg',$msg);
STemplate::assign('answers',$users);
STemplate::assign('videos',$vdo);
STemplate::assign('head_bottom',"blank.tpl");
STemplate::assign('head_bottom_add',"viewuserlinks.tpl");
STemplate::display('head1.tpl');
STemplate::display('err_msg.tpl');
STemplate::display('uprofile.tpl');
STemplate::display('footer.tpl');
?>
