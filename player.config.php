<!-- These are sample configuration properties... however, you may wish to add more! Please add as many configuration properties as you reasonably can -->
<config>
	<AUTO_START>true</AUTO_START>
	<AUTORESIZE>true</AUTORESIZE>
	
	<!-- time in seconds -->
	<BUFFER_TIME>3</BUFFER_TIME>
	
	<!-- options: Off, On -->
	<CONTROLS>On</CONTROLS>
	<LABEL_ON_START_CLICK>Press start to play</LABEL_ON_START_CLICK>
	<LOOPING>true</LOOPING>
	
	<VIDEO_FILE_PATH>videofeed.php?type=new</VIDEO_FILE_PATH>
<?php 

include("include/dbconfig.php");
include("include/config.php");
  
  $query = "SELECT A.*,B.UID,B.username FROM video A, signup B WHERE A.UID=B.UID AND A.type = 'public' ORDER BY A.VID desc LIMIT 1"; 


$db=mysql_connect ($DBHOST,$DBUSER,$DBPASSWORD) or die ('I cannot connect to the database because: ' . mysql_error());
mysql_select_db ($DBNAME); 
  
$num_rows = mysql_num_rows(mysql_query("select * from video")); 
  
$result = mysql_query($query) or die ('Query Error: ' . mysql_error()); 
while ($results = mysql_fetch_array($result)) 
{ 
$videoflv = $config['FLVDO_URL']."/";
$videourl = $config['BASE_URL']."/view_video.php?viewkey=";

print "	<VIDEOS_PATH>".$videoflv."</VIDEOS_PATH>\n"; 
print "	<VIDEOS_URL>".$videourl."</VIDEOS_URL>\n"; 

} 
mysql_close(); 
?> 
	<CURRENT_VIDEO_FILE_PATH>./flvideo/</CURRENT_VIDEO_FILE_PATH>
	<CURRENT_VIDEO_FILE_TITLE>Some title</CURRENT_VIDEO_FILE_TITLE>
	<CURRENT_VIDEO_FILE_RUNTIME>auto</CURRENT_VIDEO_FILE_RUNTIME>
	<PHP_PATH>./</PHP_PATH>
	
	<!-- number 0-100 -->
	<VOLUME>50</VOLUME>
	<skin_color></skin_color>
	<speaker_icon>0x6C6C6C</speaker_icon>
	<time_color>0x000000</time_color>
	<video_bar_back_color>0x474747</video_bar_back_color>
	<video_bar_loading_color>0xB62A2A</video_bar_loading_color>
	<video_bar_progress_color>0x8CC63E</video_bar_progress_color>
	<volume_bar_back_color>0xDBDBDB</volume_bar_back_color>
	<volume_bar_progress_color>0x515151</volume_bar_progress_color>
	
	<!-- DISABLED. PLEASE PURCHASE REBRANDING SERVICE. options: Up Left, Down Left, Up Right, Down Right -->
	<LOGO_APPEARANCE>Up Left</LOGO_APPEARANCE>
	<LOGO_CLICK_URL>http://www.motovelocity.com</LOGO_CLICK_URL>
	
	<!-- DISABLED. PLEASE PURCHASE REBRANDING SERVICE. relative path to a non-progressive JPG image -->
	<LOGO_PATH>logo.jpg</LOGO_PATH>
	<SHOW_LOGO>true</SHOW_LOGO>
	<visible>true</visible>
	<minHeight>150</minHeight>
	<minWidth>200</minWidth>
	
	<!-- number 0-100 -->
	<LOGO_ALPHA>20</LOGO_ALPHA>
	<VIDEO_ALPHA>100</VIDEO_ALPHA>
	
	<!-- options: _self, _blank, _parent, _top -->
	<LOGO_CLICK_URL_TARGET>_blank</LOGO_CLICK_URL_TARGET>
	
	<!-- number 0-100 -->
	<videoBrightness>50</videoBrightness>
	
	<!-- options: Up, Down -->
	<CONTROLS_LAYOUT>Down</CONTROLS_LAYOUT>
	
	<!-- work only if LOOPING == false. options: true, false -->
	<auto_reset_playhead>false</auto_reset_playhead>
	<video_back_color>0x000000</video_back_color>
	<panel_back_color>0x9F9F9F</panel_back_color>
	
	<!-- number 0-100 -->
	<panel_back_alpha>30</panel_back_alpha>
	
	<!-- rate_video.php uses next 3 nodes  -->
	<share_from_mail>admin@probetv.com</share_from_mail>
	<share_from_name>probetv.com</share_from_name>
	<share_text>
		<![CDATA[
	<H2>probetv.com<sup>TM</sup> - Broadcast Yourself</H2>
	<HR SIZE=1 WIDTH=90% ALIGN=left>
	<B>I want to share the following video with you:</B>
	<BR>
	<a href="http://www.probetv.com/view_video.php?viewkey=##VIDEO_NAME##">VIDEO LINK</A>
	<br>
	<B>Personal Message</B>
	<BR>
	#PERS_MESSAGE#
	<BR>
	Thanks,<BR>
	##FIRST_NAME##
	<HR SIZE=1 WIDTH=90% ALIGN=left>
	<font color="#BFBFBF">Copyright 2007 www.probetv.com, Inc.</font>	
	]]>
	</share_text>
</config>
