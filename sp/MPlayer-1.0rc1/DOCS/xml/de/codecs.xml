<?xml version="1.0" encoding="iso-8859-1"?>
<!-- synced with r20361 -->
<chapter id="codecs">
  <title>Codecs</title>

  <sect1 id="video-codecs">
    <title>Videocodecs</title>

    <para>
      Die <ulink url="../../codecs-status.html">Codecstatustabelle</ulink> ist eine vollst�ndige
      Liste aller unterst�tzten Codecs, die t�glich neu erstellt wird.
      Einige Bin�rcodecs zum Gebrauch mit MPlayer sind im
      <ulink url="http://www.mplayerhq.hu/design7/dload.html#binary_codecs">Downloadbereich</ulink>
      unserer Homepage verf�gbar.
    </para>

    <para>
      Die Allerwichtigsten:
    </para>

    <itemizedlist>
      <listitem><simpara>
          <emphasis role="bold">MPEG-1</emphasis> (<emphasis role="bold">VCD</emphasis>) und
          <emphasis role="bold">MPEG-2</emphasis> (<emphasis role="bold">DVD</emphasis>) Video
        </simpara></listitem>
      <listitem><simpara>
          Native Decoder f�r
          <emphasis role="bold">alle DivX-Varianten, 3ivX, M$ MPEG-4</emphasis>
          v1, v2 und andere MPEG-4 Varianten
        </simpara></listitem>
      <listitem><simpara>
          Nativer Decoder f�r <emphasis role="bold">Windows Media Video 7/8</emphasis>
          (<emphasis role="bold">WMV1/WMV2</emphasis>) und Win32 DLL Decoder
          f�r <emphasis role="bold">Windows Media Video 9</emphasis>
          (<emphasis role="bold">WMV3</emphasis>), beide in Gebrauch in <filename>.wmv</filename>-Dateien
        </simpara></listitem>
      <listitem><simpara>
          Nativer <emphasis role="bold">Sorenson 1 (SVQ1)</emphasis> Decoder
        </simpara></listitem>
      <listitem><simpara>
          Nativer <emphasis role="bold">Sorenson 3 (SVQ3)</emphasis> Decoder
        </simpara></listitem>
      <listitem><simpara>
          <emphasis role="bold">3ivx</emphasis> v1, v2 Decoder
        </simpara></listitem>
      <listitem><simpara>
          Cinepak und <emphasis role="bold">Intel Indeo</emphasis> Codecs (3.1,3.2,4.1,5.0)
        </simpara></listitem>
      <listitem><simpara>
          <emphasis role="bold">MJPEG</emphasis>, AVID, VCR2, ASV2 und andere Hardwareformate
        </simpara></listitem>
      <listitem><simpara>
          VIVO 1.0, 2.0, I263 und andere <emphasis role="bold">H.263(+)</emphasis>-Varianten
        </simpara></listitem>
      <listitem><simpara>
          FLI/FLC
        </simpara></listitem>
      <listitem><simpara>
          <emphasis role="bold">RealVideo 1.0 &amp; 2.0</emphasis> von
          <systemitem class="library">libavcodec</systemitem> und
          <emphasis role="bold">RealVideo 3.0 &amp; 4.0</emphasis> Codecs mittels der RealPlayer Bibliotheken
        </simpara></listitem>
      <listitem><simpara>
          Native Decoder f�r HuffYUV
        </simpara></listitem>
      <listitem><simpara>
          Verschiedene alte und simple RLE-�hnliche Formate
        </simpara></listitem>
    </itemizedlist>

    <para>
      Falls du einen Win32 Codec hast, der hier nicht aufgef�hrt ist und noch nicht unterst�tzt wird, lies bitte die
      <link linkend="codec-importing">Codec Importing HOWTO</link>
      und hilf uns, Unterst�tzung hinzuzuf�gen.
    </para>


    <sect2 id="ffmpeg" xreflabel="FFmpeg/libavcodec">
      <title>FFmpeg/libavcodec</title>

      <para>
        <ulink url="http://ffmpeg.org">FFmpeg</ulink> enth�lt <systemitem class="library">libavcodec</systemitem>,
        die f�hrende Open-Source-Bibliothek f�r Video und Audio. Sie ist in der Lage, die meisten
        Multimedia-Formate zu decodieren, normalerweise schneller als die Alternativen;
        m�glicherweise wird auch noch Unterst�tzung f�r die anderen Formate hinzugef�gt.
        Es ist der Standarddecoder f�r den Gro�teil der Codecs, die <application>MPlayer</application>
        unterst�tzt. F�r manche Formate ist au�erdem auch Encodierung mit Hilfe von
        <application>MEncoder</application> m�glich.
      </para>

    <para>
      F�r eine vollst�ndige Liste unterst�tzter Codecs besuche bitte die FFmpeg Homepage.
      Unterst�tzte <ulink url="http://ffmpeg.org/ffmpeg-doc.html#SEC19">Video-</ulink>
      und <ulink url="http://ffmpeg.org/ffmpeg-doc.html#SEC20">Audio</ulink>codecs.
    </para>

    <para>
      <application>MPlayer</application> enth�lt <systemitem class="library">libavcodec</systemitem>.
      Es gen�gt, <filename>configure</filename> laufen zu lassen und zu compilieren.
    </para>

  </sect2>


  <sect2 id="xvid">
    <title>XviD</title>

    <para>
      <ulink url="http://www.xvid.org">XviD</ulink> ist ein freier MPEG-4 ASP konformer
      Videocodec, der Encodierung in zwei Durchl�ufen und das ganze MPEG-4 ASP Spektrum
      unterst�tzt, was ihn wesentlich effizienter als den gut bekannten DivX-Codec macht.
      Er erreicht sehr gute Qualit�t und dank CPU-Optimierungen f�r die meisten modernen
      Prozessoren gute Performance.
    </para>
    <para>
      Es begann als Fork der Entwicklung des OpenDivX-Codecs.
      Dies geschah, als ProjectMayo OpenDivX zu Closed Source DivX4
      machte und die Leute, die an OpenDivX arbeiteten aber nicht zu ProjectMayo geh�rten,
      ver�rgerte. Diese riefen dann XviD ins Leben. Beide Projekte haben daher denselben Ursprung.
    </para>

    <para>
      Beachte, dass XviD nicht ben�tigt wird, um mit XviD encodiertes Video zu decodieren.
      In der Standardkonfiguration wird daf�r <systemitem class="library">libavcodec</systemitem>
      benutzt, da er h�here Geschwindigkeit bietet.
    </para>

    <procedure>
      <title>Installation von <systemitem class="library">XviD</systemitem></title>
      <para>
        Wie die meiste Open-Source-Software gibt es zwei verf�gbare Varianten:
        <ulink url="http://www.xvid.org/downloads.html">offizielle Releases</ulink>
        und die CVS-Version.
        Die CVS-Version ist f�r die Benutzung normalerweise stabil genug, da es meistens
        Fehlerbehebungen f�r Bugs enth�lt, die im Release vorhanden sind.
        Hier also, was du zu tun hast, um <systemitem class="library">XviD</systemitem>
        vom CVS mit <application>MEncoder</application> ans Laufen zu bringen
        (du ben�tigst mindestens <application>autoconf</application> 2.50,
        <application>automake</application> und <application>libtool</application>):
      </para>
      <step><para>
          <screen>cvs -z3 -d:pserver:anonymous@cvs.xvid.org:/xvid login</screen>
        </para></step>
      <step><para>
          <screen>cvs -z3 -d:pserver:anonymous@cvs.xvid.org:/xvid co xvidcore</screen>
        </para></step>
      <step><para>
          <screen>cd xvidcore/build/generic</screen>
        </para></step>
      <step><para>
          <screen>./bootstrap.sh</screen>
        </para></step>
      <step><para>
          <screen>./configure</screen>
          Du musst m�glicherweise ein paar Optionen hinzuzuf�gen (schaue dir
          die Ausgabe von <command>./configure --help</command> an).
        </para></step>
      <step><para>
          <screen>make &amp;&amp; make install</screen>
        </para></step>
      <step><para>
          Wenn du <option>--enable-divxcompat</option> angegeben hast,
          kopiere <filename>../../src/divx4.h</filename> nach
          <filename class="directory">/usr/local/include/</filename>.
        </para></step>
      <step><para>
          Compiliere <application>MPlayer</application> erneut mit
          <option>--with-xvidlibdir=<replaceable>/Pfad/zu/</replaceable>libxvidcore.a</option>
          <option>--with-xvidincdir=<replaceable>/Pfad/zu/</replaceable>xvid.h</option>.
        </para></step>
    </procedure>
  </sect2>


  <sect2 id="codec-x264">
    <title>x264</title>

    <sect3 id="codec-x264-whatis">
      <title>Was ist x264?</title>
      <para>
        <ulink url="http://developers.videolan.org/x264.html"><systemitem class="library">x264</systemitem></ulink>
        ist eine Bibliothek f�r die
        Erstellung von H.264-Videostreams.
        Es ist nicht 100% vollst�ndig, hat momentan aber zumindest eine Art
        Unterst�tzung f�r die meisten H.264-Features, die Einflu� auf die Qualit�t haben.
        Es gibt au�erdem viele fortgeschrittene Features in der H.264-Spezifikation,
        die mit Videoqualit�t per se nichts zu tun haben; viele davon sind in
        <systemitem class="library">x264</systemitem> nicht implementiert.
      </para>

      <itemizedlist>
        <title>Encoder-Features</title>
        <listitem><para>CAVLC/CABAC</para></listitem>
        <listitem><para>mehrere Referenzen</para></listitem>
        <listitem><para>
            Intra: alle Macroblock-Typen (16x16, 8x8, and 4x4 mit allen Vorhersagen)
          </para></listitem>
        <listitem><para>
            Inter P: Alle Partitionen (von 16x16 bis runter zu 4x4)
          </para></listitem>
        <listitem><para>
            Inter B: Partitionen von 16x16 runter zu 8x8 (inklusive SKIP/DIRECT)
          </para></listitem>
        <listitem><para>
            Bitratenkontrolle: konstanter Quantisierungsparameter, konstante Bitrate,
            einer oder mehrere Durchl�ufe mit durchschnittlicher Bitrate (optional VBV)
          </para></listitem>
        <listitem><para>Szenenwechselerkennung</para></listitem>
        <listitem><para>Adaptive Platzierung von B-Frames</para></listitem>
        <listitem><para>B-Frames als Referenzen / bieliebige Framereihenfolge</para></listitem>
        <listitem><para>8x8 und 4x4 adaptive r�umliche Transformierung</para></listitem>
        <listitem><para>Verlustfreier Modus</para></listitem>
        <listitem><para>Benutzerdefinierte Quantisierungsmatrizen</para></listitem>
        <listitem><para>Parallele Encodierung mehrerer Scheiben</para></listitem>
      </itemizedlist>

    </sect3>

    <sect3 id="codec-h264-whatis">
      <title>Was ist H.264?</title>
      <para>
        H.264 ist ein Name f�r einen neuen digitalen Videocodec, der von
        der ITU und MPEG zusammen entwickelt wurde.
        Etwas umst�ndlicher kann man ihn auch "ISO/IEC 14496-10" oder "MPEG-4 Part 10"
        nennen. H�ufiger wird er als "MPEG-4 AVC" oder einfach "AVC" bezeichnet.
      </para>
      <para>
        Wie auch immer du ihn nennst, H.264 ist es eventuell wert, ausprobiert
        zu werden, da er typischerweise die Qualit�t von MPEG-4 ASP bei 5%-30%
        Einsparung der Bitrate erreicht.
        Tats�chliche Ergebnisse werden sowohl vom Quellmaterial als auch dem
        Encoder abh�ngen.
        Die Gewinne durch die Benutzung von H.264 kommen nicht umsonst: Decodierung
        von H.264-Streams hat anscheinend heftige CPU- und Speicheranforderungen.
        Beispielsweise ben�tigt ein Athlon mit 1733 MHz f�r ein 1500kbps-H.264-Video
        in DVD-Aufl�sung um die 35% CPU-Leistung f�r die Decodierung.
        Im Vergleich dazu wird f�r einen 1500kbps MPEG-4 ASP Stream um die 10% CPU-Leistung gebraucht.
        Dies bedeutet, dass Decodierung von High-Definition-Streams f�r die
        meisten Benutzer au�er Frage steht.
        Es bedeutet auch, dass sogar ein ordentlicher DVD-Rip auf Prozessoren
        langsamer als 2.0 GHz oder so ins Stocken geraten kann.
      </para>
      <para>
        Zumindest bei <systemitem class="library">x264</systemitem> sind die Anforderungen
        f�r die Encodierung nicht so viel schlimmer als das, was du von 
        MPEG-4 ASP bereits kennst.
        Beispielsweise l�uft eine DVD-Encodierung auf einem 1733 MHz Athlon bei
        etwa 5-15 fps.
      </para>
      <para>
        Dieses Dokument hat nicht die Absicht, die Details von H.264 zu erkl�ren,
        wenn du aber an einer ausf�hrlichen �bersicht interessiert bist, kannst du dir
        <ulink url="http://www.cdt.luth.se/~peppar/kurs/smd151/spie04-h264OverviewPaper.pdf">The H.264/AVC Advanced Video Coding Standard: Overview and Introduction to the Fidelity Range Extensions</ulink>
        durchlesen.
      </para>
    </sect3>

    <sect3 id="codec-x264-playback">
      <title>Wie kann ich H.264-Videos mit <application>MPlayer</application> abspielen?</title>
      <para>
        <application>MPlayer</application> benutzt den H.264-Decoder von 
        <systemitem class="library">libavcodec</systemitem>.
        <systemitem class="library">libavcodec</systemitem> hat minimal nutzbare
        H.264-Decodierung seit etwa Juli 2004, seitdem wurden jedoch gro�e �nderungen
        und Verbesserungen implementiert, sowohl hinsichtlich mehr unterst�tzten Funktionen
        als auch CPU-Last.
        Um einfach sicher zu sein, ist es immer eine gute Idee, einen aktuellen
        Subversion-Checkout zu verwenden.
      </para>
      <para>
        Eine schnelle und einfache Methode, festzustellen, ob k�rzlich �nderungen am
        H.264-Decoder von <systemitem class="library">libavcodec</systemitem> gemacht
        wurden, wirf einen Blick auf
        <ulink url="http://svn.mplayerhq.hu/ffmpeg/trunk/libavcodec/h264.c?view=log">das Web-Interface des Subversion-Repository von FFmpeg</ulink>.
      </para>
    </sect3>

    <sect3 id="codec-x264-encode">
      <title>Wie kann ich Videos mit <application>MEncoder</application> und <systemitem class="library">x264</systemitem> encodieren?</title>
      <para>
        Wenn du den Subversion-Client installiert hast, erh�ltst du die aktuellen
        Sourcen mit folgendem Befehl:
        <screen>svn co svn://svn.videolan.org/x264/trunk x264</screen>
        <application>MPlayer</application>-Sourcen werden immer aktualisiert, wenn sich
        die API von <systemitem class="library">x264</systemitem> �ndert, daher ist
        es immer gut, auch <application>MPlayer</application> von Subversion zu benutzen.
        M�glicherweise �ndert sich das, wenn und falls es ein
        <systemitem class="library">x264</systemitem>-"Release" gibt.
        Bis dahin sollte <systemitem class="library">x264</systemitem>
        hinsichtlich der Programmierschnittstelle als sehr unstabil betrachtet werden.
      </para>
      <para>
        <systemitem class="library">x264</systemitem> wird gebaut und installiert
        nach Standardmethode:
        <screen>./configure &amp;&amp; make &amp;&amp; sudo make install</screen>
        Dies installiert libx264.a nach /usr/local/lib, und x264.h landet in
        /usr/local/include.

        Mit der <systemitem class="library">x264</systemitem>-Bibliothek und dem Header
        in den Standardverzeichnissen ist es leicht, <application>MPlayer</application>
        mit <systemitem class="library">x264</systemitem>-Unterst�tzung zu bauen.
        F�hre die Standardformel aus:
        <screen>./configure &amp;&amp; make &amp;&amp; sudo make install</screen>
        Das configure-Script wird automatisch erkennen, dass du die Bedingungen
        f�r <systemitem class="library">x264</systemitem> erf�llst.
      </para>
    </sect3>
  </sect2>
</sect1>

<!-- ********** -->

<sect1 id="audio-codecs">

  <title>Audiocodecs</title>
  <itemizedlist>
    <title>Die allerwichtigsten Audiocodecs:</title>
    <listitem><simpara>
        MPEG layer 1/2/3 (MP1/2/3) Audio (<emphasis role="bold">nativer</emphasis>
        Decoder, mit MMX/SSE/3DNow! Optimierung)
      </simpara></listitem>
    <listitem><simpara>
        Windows Media Audio 7 und 8 (alias WMAv1 und WMAv2) (<emphasis role="bold">nativer</emphasis> Decoder, mit
        <link linkend="ffmpeg"><systemitem class="library">libavcodec</systemitem></link>)
      </simpara></listitem>
    <listitem><simpara>
        Windows Media Audio 9 (WMAv3) (unter Verwendung der DMO-DLL)
      </simpara></listitem>
    <listitem><simpara>
        AC3 Dolby Audio (<emphasis role="bold">nativer</emphasis> Decoder, mit
        MMX/SSE/3DNow! Optimierung)
      </simpara></listitem>
    <listitem><simpara>
        AC3-Passthrough unter Verwendung der Soundkartenhardware
      </simpara></listitem>
    <listitem><simpara>
        AAC
      </simpara></listitem>
    <listitem><simpara>
        Ogg Vorbis Audiocodec (<emphasis role="bold">native</emphasis> Bibliothek)
      </simpara></listitem>
    <listitem><simpara>
        RealAudio: DNET (AC3 mit niedriger Bitrate), Cook, Sipro and ATRAC3
      </simpara></listitem>
    <listitem><simpara>
        QuickTime: Qualcomm und QDesign Audio Decoder
      </simpara></listitem>
    <listitem><simpara>
        VIVO Audio (g723, Vivo Siren)
      </simpara></listitem>
    <listitem><simpara>
        Voxware Audio (unter Verwendung der DirectShow-DLL)
      </simpara></listitem>
    <listitem><simpara>
        alaw und ulaw, verschiedene gsm, adpcm und pcm Formate und andere einfache alte
        Audiocodecs
      </simpara></listitem>
    <listitem><simpara>
        Adaptive Multi-Rate (AMR) Sprachcodecs
      </simpara></listitem>
  </itemizedlist>


  <sect2 id="swac3">
    <title>Software-AC3-Decoder</title>

    <para>
      Das ist der Standarddecoder f�r Dateien, die AC3-Audio enthalten.
    </para>

    <para>
      Der AC3-Decoder kann Audio f�r zwei, vier oder sechs Lautsprecher ausgeben.
      Wenn er f�r sechs Lautsprecher konfiguriert wurde, stellt der Decoder dem
      Audiotreiber alle Kan�le separat zur Verf�gung und erlaubt so volle
      "Surround"-Erfahrung ohne einen externen AC3 Decoder, der gebraucht wird,
      um hwac3 zu benutzen.
    </para>

    <para>
      Benutze die Option <option>-channels</option>, um die Anzahl der Kan�le
      anzugeben. Mit <option>-channels 2</option> wird Stereo-Klang erzeugt.
      F�r eine 4-Kanal-Ausgabe (links vorne, rechts vorne, links Surround und
      rechts Surround) verwende <option>-channels 4</option>. In diesem
      Fall wird der Kanal Center gleichm��ig auf die vorderen Kan�le verteilt.
      <option>-channels 6</option> gibt alle AC3-Kan�le aus, wie sie codiert
      wurden - in der Reihenfolge links vorne, rechts vorne, links Surround, rechts
      Surround, Center und LFE.
    </para>

    <para>
      Standard sind zwei Ausgabekan�le.
    </para>

    <para>
      Um mehr als zwei Ausgabekan�le verwenden zu k�nnen, muss OSS verwendet werden,
      und die Soundkarte muss die entsprechende Anzahl an Kan�len mittels ioctl
      SNDCTL_DSP_CHANNELS unterst�tzen. Ein funktionierender Treiber ist emu10k1
      (wird f�r Soundblaster Live! Karten benutzt) seit August 2001
      (Neuere ALSA-Versionen sollten auch funktionieren).
    </para>
  </sect2>


  <sect2 id="hwac3">
    <title>Hardware-AC3-Decoder</title>
    <para>
      Ben�tigt wird eine AC3 f�hige Soundkarte mit einem Digitalausgang (S/PDIF).
      Der Treiber der Karte muss das AFMT_AC3 Format korrekt unterst�tzen (wie
      z.B. C-Media). Verbinde dann den Hardware AC3 Decoder mit dem S/PDIF Ausgang
      und benutze <option>-ac hwac3</option>. Die Unterst�tzung ist experimentell,
      funktioniert aber z.B. mit C-Media Karten und Soundblaster Live! mit ALSA
      (aber nicht OSS) Treibern, sowie mit DXR3/Hollywood+ MPEG Decoder Karten.
    </para>
  </sect2>


  <sect2 id="libmad">
    <title>Unterst�tzung f�r libmad</title>

    <para>
      <ulink url="http://www.underbit.com/products/mad/">libmad</ulink> ist
      eine Integer-MPEG-Audiodecoder-Bibliothek f�r mehrere Plattformen, die intern mit
      24-bittigem PCM arbeitet. Sie funktioniert nicht sehr gut mit besch�digten
      Dateien und hat manchmal Probleme mit Spr�ngen (seeks), kann aber auf
      Platformen ohne FPU (z.B. <link linkend="arm">ARM</link>) schneller
      sein als mp3lib.
    </para>

    <para>
      Wenn libman bei dir korrekt installiert ist, wird <filename>configure</filename>
      dies bemerken, und Unterst�tzung f�r MPEG-Audiodecodierung via
      <systemitem class="library">libmad</systemitem> wird automatisch eingebaut.
    </para>
  </sect2>

  <sect2 id="hwmpa">
    <title>Hardware-MPEG-Audiodecoder</title>

    <para>
      Dieser Codec (ausgew�hlt durch <option>-ac hwmpa</option>) reicht
      MPEG-Audiopakete an Hardwaredecoder durch, wie sie z.B. auf
      voll ausgestatteten DVB-Karten und DXR2-Adaptern existieren.
      Es bringt nichts, ihn mit einem anderen Ger�t (wie z.B. OSS und ALSA)
      zu verwenden - das f�hrt lediglich zu Rauschen.
    </para>
  </sect2>


  <sect2 id="aac">
    <title>AAC Codec</title>
    <para>
      AAC (Advanced Audio Coding) ist ein Audiocodec, der vorwiegend
      in MOV- und MP4-Dateien vorkommt. Ein Open-Source-Decoder namens
      FAAD ist auf <ulink url="http://www.audiocoding.com"/>
      verf�gbar. <application>MPlayer</application> enth�lt
      eine CVS Version von libfaad 2.1 beta, so dass es nicht extra
      installiert werden muss.
    </para>

    <para>
      Um die externe FAAD-Bibliothek verwenden zu k�nnen (z.B. um mit gcc 3.2
      compilieren zu k�nnen, der die interne Version nicht �bersetzt), muss
      die Bibliothek von der
      <ulink url="http://www.audiocoding.com/modules/mydownloads/">Download Seite</ulink>
      heruntergeladen und installiert werden. Anschlie�end muss die Option
      <option>--enable-faad-external</option> an <filename>configure</filename>
      �bergeben werden, damit die externe Bibliothek erkannt wird.
      Es ist lediglich erforderlich, vorher libfaad zu installieren, und zwar so:
      <screen>
cd faad2/
sh bootstrap
./configure
cd libfaad
make
make install<!--
      --></screen>
      Bin�rpakete sind auf audiocoding.com nicht erh�ltlich, Debian-Pakete
      k�nnen aber mit (apt-)get von
      <ulink url="http://www.debian-multimedia.org/">Christian Marillat's Homepage</ulink>, Mandrake/Mandriva RPMs von
      <ulink url="http://plf.zarb.org">P.L.F</ulink> und RedHat RPMs
      von <ulink url="http://greysector.rangers.eu.org/">Grey Sector</ulink>
      heruntergeladen werden.
    </para>
  </sect2>

  <sect2 id="amr">
    <title>AMR Codecs</title>
    <para>
      Adaptive Multi-Rate Sprachcodec, wird in 3G (UMTS) Mobiltelephonen verwendet.
      Die Referenzimplementierung ist auf
      <ulink url="http://www.3gpp.org">The 3rd Generation Partnership Project</ulink>
      erh�ltlich (frei - wie in Freibier - f�r private Benutzung).
    </para>
    <para>
      F�r Unterst�tzung m�ssen die Codecs
      <ulink url="http://www.3gpp.org/ftp/Specs/latest/Rel-6/26_series/26104-610.zip">AMR-NB</ulink>
      und
      <ulink url="http://www.3gpp.org/ftp/Specs/latest/Rel-6/26_series/26204-600.zip">AMR-WB</ulink>
      heruntergeladen und in dasselbe Verzeichnis wie
      <application>MPlayer</application> verschoben werden.
      Anschlie�end folgende Befehle ausf�hren:
      <screen>
unzip 26104-610.zip
unzip 26104-610_ANSI_C_source_code.zip
mv c-code libavcodec/amr_float
unzip 26204-600.zip
unzip 26204-600_ANSI-C_source_code.zip
mv c-code libavcodec/amrwb_float<!--
      --></screen>
      Befolge danach einfach das <link linkend="features">Standardvorgehen f�r die Compilierung</link> von
      <application>MPlayer</application>.
    </para>
  </sect2>

</sect1>

<!-- ********** -->

<sect1 id="codec-importing">
  <title>HOWTO Win32-Codecs-Importierung</title>

  <!-- TODO: a short paragraph of text would be nice here... -->

  <sect2 id="vfw-codecs">
    <title>VFW-Codecs</title>

    <para>
      VFW (Video for Windows) ist die alte Video-API von Windows. Deren Codecs hat die
      Dateinamenerweiterung <filename>.DLL</filename> oder (selten) <filename>.DRV</filename>.
      Wenn <application>MPlayer</application> bei der Wiedergabe deines AVIs fehlschl�gt
      mit einer Meldung wie:
      <screen>UNKNOWN video codec: HFYU (0x55594648)</screen>
    </para>

    <para>
      ..bedeutet dies, dass dein AVI mit einem Codec encodiert ist, der den fourcc HFYU
      (HFYU = HuffYUV-Codec, DIV3 = DivX Low Motion, etc.) hat. Da du dies jetzt wei�t,
      musst du herausfinden, welche DLL Windows l�dt, um diese Datei abzuspielen. In
      unserem Fall enth�lt die <filename>system.ini</filename> diese Information in einer
      Zeile, die so aussieht:
      <programlisting>VIDC.HFYU=huffyuv.dll</programlisting>
    </para>

    <para>
      Du ben�tigst also die Datei <filename>huffyuv.dll</filename>. Beachte, dass
      Audiocodecs mit dem MSACM-Pr�fix angegeben werden:
      <programlisting>msacm.l3acm=L3codeca.acm</programlisting>
    </para>

    <para>
      Dies ist der MP3-Codec. Da du nun alle n�tigen Informationen hast
      (fourcc, Codecdatei, Beispiel-AVI), gib eine Anfrage f�r Codecunterst�tzung per
      Mail auf, und lade diese Dateien auf den FTP-Server hoch:
      <systemitem role="url">
        ftp://upload.mplayerhq.hu/MPlayer/incoming/[Codecname]/
      </systemitem>
    </para>

    <note><para>
        Suche nach dieser Information unter Windows NT/2000/XP in der Registry,
        suche also z.B. nach &quot;VIDC.HFYU&quot;. Um herauszufinden, wie man das
        macht, schaue dir die folgende Methode zu DirectShow an.
      </para></note>
  </sect2>


  <sect2 id="dshow-codecs">
    <title>DirectShow-Codecs</title>

    <para>
      DirectShow ist die neuere Video-API, die noch schlechter ist als ihr Vorg�nger.
      Dinge sind schwieriger geworden mit DirectShow, da
      <itemizedlist>
        <listitem><simpara>
            <filename>system.ini</filename> nicht die ben�tigten Informationen enth�lt,
            die statt dessen in der Registry gespeichert ist.
          </simpara></listitem>
        <listitem><simpara>
            wir die GUID des Codecs ben�tigen.
          </simpara></listitem>
      </itemizedlist>
    </para>

    <procedure>
      <title>Neue Methode:</title>
      <para>
        Benutzung von <application>Microsoft GraphEdit</application> (schnell)
      </para>
      <step><para>
          Beziehe <application>GraphEdit</application> entweder aus dem DirectX-SDK
          oder von <ulink url="http://doom9.net">doom9</ulink>.
        </para></step>
      <step><para>
          Starte <command>graphedit.exe</command>.
        </para></step>
      <step><para>
          W�hle aus dem Men� Graph -> Insert Filters.
        </para></step>
      <step><para>
          Klappe den Eintrag <systemitem>DirectShow Filters</systemitem> aus.
        </para></step>
      <step><para>
          W�hle den richtigen Codecnamen und klappe den Eintrag aus.
        </para></step>
      <step><para>
          Schaue im Eintrag <systemitem>DisplayName</systemitem> nach dem Text in den
          geschweiften Klammern hinter dem Backslash und notiere ihn
          (f�nf mit Bindestrich getrennte Bl�cke, die GUID).
        </para></step>
      <step><para>
          Die Codecbin�rdatei ist die Datei, die im Eintrag
          <systemitem>Filename</systemitem> angegeben wird.
        </para></step>
    </procedure>

    <note>
      <para>
        Wenn dort kein <systemitem>Filename</systemitem> ist und
        <systemitem>DisplayName</systemitem> etwas wie 
        <systemitem>device:dmo</systemitem> enth�lt, handelt es sich um einen DMO-Codec.
      </para>
    </note>

    <procedure>
      <title>Alte Methode:</title>
      <para>
        Nimm einen tiefen Atemzug und beginne die Suche in der Registry...
      </para>
      <step><para>
          Starte <command>regedit</command>.
        </para></step>
      <step><para>
          Dr�cke <keycap>Strg</keycap>+<keycap>F</keycap>, deaktiviere die ersten beiden
          Checkboxen und aktiviere die dritte. Gib den fourcc des Codecs ein (z.B.
          <userinput>TM20</userinput>).
        </para></step>
      <step><para>
          Du solltest ein Feld sehen, das den Pfad und den Dateinamen (z.B.
          <filename>C:\WINDOWS\SYSTEM\TM20DEC.AX</filename>) enth�lt.
        </para></step>
      <step><para>
          Da du jetzt die Datei hast, ben�tigen wir die GUID. Probiere erneut die
          Suche, nun aber nach dem Codecnamen, nicht dem fourcc. Dessen Name kann man
          herausfinden, indem man w�hrend der Wiedergabe mit Media Player den Eintrag von
          <guimenu>Datei</guimenu> -&gt; <guisubmenu>Eigenschaften</guisubmenu> -&gt;
          <guimenuitem>Erweitert</guimenuitem> �berpr�ft.
          Wenn dies nicht klappt, hast du Pech. Versuche raten (suche z.B. nach TrueMotion).
        </para></step>
      <step><para>
          Wenn die GUID gefunden wurde, solltest du das Feld <guilabel>FriendlyName</guilabel>
          und <guilabel>CLSID</guilabel> sehen. Notiere die 16-Byte-CLSID, das ist die
          GUID, die wir brauchen.
        </para></step>
    </procedure>

    <note>
      <para>
        Wenn die Suche fehlschl�gt, probiere die Aktivierung aller Checkboxen. Es kann sein,
        dass du falsche Treffer erh�ltst, aber vielleicht hast du Gl�ck...
      </para>
    </note>

    <para>
      Da du nun alle n�tigen Informationen hast (fourcc, Codecdatei, Beispiel-AVI),
      gib eine Anfrage f�r Codecunterst�tzung per Mail auf, und lade diese Dateien
      auf den FTP-Server hoch:
      <systemitem role="url">
        ftp://upload.mplayerhq.hu/MPlayer/incoming/[Codecname]/
      </systemitem>
    </para>

    <para>
      Wenn du einen Codec selbst hinzuf�gen m�chtest, lies
      <ulink url="http://www.mplayerhq.hu/MPlayer/DOCS/tech/codecs.conf.txt">DOCS/tech/codecs.conf.txt</ulink>.
    </para>

  </sect2>
</sect1>

</chapter>
