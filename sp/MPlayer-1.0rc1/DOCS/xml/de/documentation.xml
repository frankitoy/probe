<?xml version="1.0" encoding="iso-8859-1"?>
<!-- in sync with r20085 -->

<bookinfo id="toc">
  <title><application>MPlayer</application> - Movie Player</title>
  <subtitle>
    <ulink url="http://www.mplayerhq.hu"></ulink>
  </subtitle>
  <date>24. M�rz 2003</date>
  <copyright>
    <year>2000</year>
    <year>2001</year>
    <year>2002</year>
    <year>2003</year>
    <year>2004</year>
    <year>2005</year>
    <year>2006</year>
    <holder>MPlayer-Team</holder>
  </copyright>
  <!--
      <legalnotice>
        <title>License</title>
        <para>This program is free software; you can redistribute it and/or modify
          it under the terms of the GNU General Public License as published by the
          Free Software Foundation; either version 2 of the License, or (at your
          option) any later version.</para>

        <para>This program is distributed in the hope that it will be useful, but
          WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
          or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
          for more details.</para>

        <para>You should have received a copy of the GNU General Public License
          along with this program; if not, write to the Free Software Foundation,
          Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.</para>
      </legalnotice>
      -->
    </bookinfo>


    <preface id="howtoread">
      <title>Wie diese Dokumentation gelesen werden soll</title>

      <para>
        Wenn du zum ersten Mal installierst: Lies in jedem Fall alles von hier bis zum
        Ende des Installationsabschnitts, und folge den Links, die du findest. Wenn Fragen
        bleiben, gehe zur�ck zum <link linkend="toc">Inhaltsverzeichnis</link> und suche nach
        dem Thema, lies die <xref linkend="faq"/> oder versuche, die Dateien zu greppen.
        Die meisten Fragen sollten irgendwo hier beantwortet werden, und nach dem Rest wurde
        vermutlich auf den
        <ulink url="http://www.mplayerhq.hu/design7/info.html#mailing_lists">Mailing-Listen</ulink> gefragt.
        <!-- FIXME: This refers to nonexistent links
             Checke die
             <ulink url="http://lists.mplayerhq.hu/cgi-bin/s-arch.cgi">Archive</ulink>,
             dort gibt es viele wertvolle Informationen.
             -->
      </para>
    </preface>


    <chapter id="intro">
      <title>Einf�hrung</title>

      <para>
        <application>MPlayer</application> ist ein Movie-Player f�r Linux (der auch auf vielen
        anderen Unices und <emphasis role="bold">nicht-x86</emphasis>-Architekturen l�uft, siehe
        <xref linkend="ports"/>). Er spielt die meisten Dateien in den Formaten MPEG, VOB, AVI,
        OGG/OGM, VIVO,  ASF/WMA/WMV, QT/MOV/MP4, FLI, RM, NuppelVideo, yuv4mpeg, FILM, RoQ, PVA,
        Matroska-Dateien, unterst�tzt von vielen eingebauten, XAnim-, RealPlayer und Win32-DLL-Codecs.
        Es k�nnen auch <emphasis role="bold">VideoCDs, SVCDs, DVDs, 3ivx-, RealMedia-, Sorenson-, Theora-</emphasis>
        und <emphasis role="bold">MPEG-4 (DivX)</emphasis> - Filme angeschaut werden.

        Ein weiteres gro�es Feature von <application>MPlayer</application> ist die F�lle an
        unterst�tzten Ausgabetreibern. Er funktioniert mit X11, Xv, DGA, OpenGL, SVGAlib,
        fb-dev, AAlib, libcaca und DirectFB, du kannst ihn aber auch mit GGI und SDL (und damit
        allen von SDL unterst�tzen Treibern), sowie mit einigen kartenspezifischen Low-Level-Treibern
        (f�r Matrox, 3Dfx und Radeon, Mach64, Permedia3) benutzen! Die meisten von ihnen unterst�tzen
        Software- oder Hardwareskalierung, so dass die Vollbildwiedergabe kein Problem ist.
        <application>MPlayer</application> unterst�tzt die Wiedergabe mittels einiger
        Hardware-MPEG-Decoderkarten wie der DVB, DXR2 und DXR3/Hollywood+ benutzen.
        Und was ist mit diesen sch�nen, gro�en, kantengegl�tteten und schattierten Untertiteln
        (<emphasis role="bold">14 unterst�tzte Typen</emphasis>) mit Europ�ischen/ISO 8859-1,2
        (Ungarisch, Englisch, Tschechisch usw.), Kryllisch und Koreanische Schriftarten, und
        On-Screen-Display (OSD)?
      </para>

      <para>
        Der Player ist superstabil bei der Wiedergabe von besch�digten MPEG-Dateien (n�tzlich
        f�r manche VCDs) und spielt schlechte AVI-Dateien ab, die mit dem ber�hmten
        Windows Media Player nicht abgespielt werden k�nnen. Selbst AVI-Dateien ohne Index-Abschnitt
        sind abspielbar, und du kannst den Index mit der Option <option>-idx</option>
        tempor�r neu generieren, oder permanent mit <application>MEncoder</application>,
        was Spulen erm�glicht! Wie du siehst, sind Stabilit�t und Qualit�t die
        wichtigsten Dinge, die Geschwindigkeit ist jedoch auch erstaunlich. Es gibt
        au�erdem ein m�chtiges Filtersystem f�r die Manipulation von Video und Ton.
      </para>

      <para>
        <application>MEncoder</application> (<application>MPlayer</application>s Movie
        Encoder) ist ein einfacher Film-Encoder, der so ausgelegt ist, von
        <application>MPlayer</application>-abspielbaren Formaten
        (<emphasis role="bold">AVI/ASF/OGG/DVD/VCD/VOB/MPG/MOV/VIV/FLI/RM/NUV/NET/PVA</emphasis>)
        in andere <application>MPlayer</application>-abspielbare Formate (siehe unten)
        zu encodieren. Er kann mit verschiedenen Codecs encodieren, zum Beispiel
        <emphasis role="bold">MPEG-4 (DivX4)</emphasis> (ein oder zwei Durchl�ufe),
        <systemitem class="library">libavcodec</systemitem>, und
        <emphasis role="bold">PCM/MP3/VBR MP3</emphasis>-Audio.
      </para>


      <itemizedlist>
        <title><application>MEncoder</application> Features</title>
        <listitem>
          <simpara>
            Encodierung zu der weitreichenden Menge Dateiformate und Decoder von
            <application>MPlayer</application>
          </simpara>
        </listitem>
        <listitem>
          <simpara>
            Encodierung zu allen Codecs von FFmpegs
            <link linkend="ffmpeg"><systemitem class="library">libavcodec</systemitem></link>
          </simpara>
        </listitem>
        <listitem>
          <simpara>
            Videoencodierung von <emphasis role="bold">V4L-kompatiblen TV-Empf�ngern</emphasis>
          </simpara>
        </listitem>
        <listitem>
          <simpara>
            Encodierung/Multiplexing von interleaved AVI-Dateien mit ordentlichem Index
          </simpara>
        </listitem>
        <listitem>
          <simpara>
            Erstellung von Dateien aus externen Audiostreams
          </simpara>
        </listitem>
        <listitem>
          <simpara>
            Encodierung in 1, 2 oder 3 Durchl�ufen
          </simpara>
        </listitem>
        <listitem>
          <para>
            <emphasis role="bold">VBR</emphasis>-MP3-Audio
            <important><simpara>
                VBR-MP3-Audio wird von Windows-Playern nicht immer sauber wiedergegeben!
              </simpara></important>
          </para>
        </listitem>
        <listitem>
          <simpara>
            PCM-Audio
          </simpara>
        </listitem>
        <listitem>
          <simpara>
            Streamkopien
          </simpara>
        </listitem>
        <listitem>
          <simpara>
            Input-A/V-Synchronisation (PTS-basiert, kann mit der Option
            <option>-mc 0</option> deaktiviert werden)
          </simpara>
        </listitem>
        <listitem>
          <simpara>
            fps-Korrektur mit der Option <option>-ofps</option> (n�tzlich bei Encodierung von
            30000/1001 fps VOB zu 24000/1001 fps AVI)
          </simpara>
        </listitem>
        <listitem>
          <simpara>
            Benutzung unseres sehr m�chtigen Filtersystems (abschneiden, expandieren, spiegeln,
            nachbearbeiten, rotieren, skalieren, rgb/yuv-Konvertierung)
          </simpara>
        </listitem>
        <listitem>
          <simpara>
            Kann DVD/VOBsub- <emphasis role="bold">UND</emphasis> Textuntertitel in die
            Ausgabedatei encodieren
          </simpara>
        </listitem>
        <listitem>
          <simpara>
            Kann DVD-Untertitel in das VOBsub-Format rippen
          </simpara>
        </listitem>
      </itemizedlist>

      <itemizedlist>
        <title>Geplante Features</title>
        <listitem>
          <simpara>
            Noch breiteres Feld an verf�gbaren En-/Decodierungsformaten/-codecs
            (erstellen von VOB-Dateien mit DivX4/Indeo5/VIVO-Streams :).
          </simpara>
        </listitem>
      </itemizedlist>

      <!-- FIXME: Lizenz sollte in bookinfo sein -->
      <para>
        <application>MPlayer</application> und <application>MEncoder</application> k�nnen
        weitergegeben werden unter den Bedingungen der GNU General Public License Version 2.
      </para>

    </chapter>

&install.xml;

&usage.xml;
&cd-dvd.xml;
&faq.xml;

&containers.xml;
&codecs.xml;
&video.xml;
&audio.xml;
&tvinput.xml;
&radio.xml;

&ports.xml;
&mencoder.xml;
&encoding-guide.xml;
&bugreports.xml;
&bugs.xml;
&skin.xml;
&history.xml;
