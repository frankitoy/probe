<?xml version="1.0" encoding="iso-8859-1"?>
<!-- synced with r20379 -->
<chapter id="cd-dvd">
  <title>CD/DVD Nutzung</title>

  <sect1 id="drives">
    <title>CD/DVD-Laufwerke</title>

    <para>
      Moderne CD-ROM-Laufwerke k�nnen sehr hohe Geschwindigkeiten
      erreichen. Jedoch sind einige CD-ROM-Laufwerke in der Lage, mit gedrosselter
      Geschwindigkeit zu laufen. Es gibt verschiedene Gr�nde, in Erw�gung zu ziehen,
      die Geschwindig eines CD-ROM-Laufwerks zu �ndern:
    </para>

    <itemizedlist>
      <listitem>
        <para>
          Es gibt Berichte �ber Lesefehler bei hohen Geschwindigkeiten, besonders
          bei schlecht gepressten CD-ROMs. Reduzierung der Geschwindigkeit kann
          unter diesen Umst�nden Datenverlust verhindern.
        </para>
      </listitem>

      <listitem>
        <para>
          Viele CD-ROM-Laufwerke sind nervend laut. Eine geringere Geschwindigkeit kann
          die Ger�sche reduzieren.
        </para>
      </listitem>
    </itemizedlist>

    <sect2 id="drives_linux">
      <title>Linux</title>

      <para>
        Du kannst die Geschwindigkeit von IDE CD-ROM-Laufwerken mit
        <command>hdparm</command>, <command>setcd</command> oder
        <command>cdctl</command> reduzieren. Dies funktioniert wie folgt:
        <screen>hdparm -E <replaceable>[Geschwindigkeit]</replaceable> <replaceable>[CD-ROM-Ger�t]</replaceable></screen>
        <screen>setcd -x <replaceable>[Geschwindigkeit]</replaceable> <replaceable>[CD-ROM-Ger�t]</replaceable></screen>
        <screen>cdctl -bS <replaceable>[Geschwindigkeit]</replaceable></screen>
      </para>

      <para>
        Wenn du SCSI-Emulation benuzt, musst du die Einstellungen unter Umst�nden am
        echten IDE-Ger�t vornehmen und nicht am emuliertem SCSI-Ger�t.
      </para>

      <para>
        Wenn du �ber root-Rechte verf�gst, kann das folgende Kommando
        ebenso helfen:
        <screen>echo file_readahead:2000000 &gt; /proc/ide/<replaceable>[CD-ROM-Ger�t]</replaceable>/settings</screen>
      </para>

      <para>
        <!-- TODO: 1) aehm Ich glaube nicht das das (oben) als 2 MB interpretiert wird.
             Duerften Bytes sein -->
        Dies setzt die Menge der vorausgehend gelesenen Daten auf 2MB, was bei
        verkratzten CD-ROMs hilft.
        Wenn du dies zu hoch setzt, wird das Laufwerk dauernd anlaufen und wieder langsamer werden;
        dies wird die Leistung dramtisch verschlechtern.
        Es wird ebenso empfohlen, dass du dein CD-ROM-Laufwerk mit <command>hdparm</command>
        konfigurierst:
        <screen>hdparm -d1 -a8 -u1 <replaceable>[CD-ROM-Ger�t]</replaceable></screen>
      </para>

      <para>
        Dies aktiviert DMA-Zugriff, Read-ahead (vorausgehendes Lesen) und IRQ-Unmasking
        (lies die <command>hdparm</command> Manpage f�r eine ausf�hrliche Erkl�rung).
      </para>

      <para>
        Wir verweisen hier auf
        &quot;<filename>/proc/ide/<replaceable>[CD-ROM-Ger�t]</replaceable>/settings</filename>&quot;
        f�r Feineinstellungen an deinem CD-ROM.
      </para>

      <para>
        SCSI-Laufwerke haben kein einheitliches Verfahren, diese Parameter zu
        setzen. (Kennst du einen? Berichte ihn uns!) Es gibt ein Tool, welches mit
        <ulink url="http://das.ist.org/~georg/">Plextor SCSI-Laufwerken</ulink>
        funktioniert.
      </para>

    </sect2>


    <sect2 id="drives_freebsd">
      <title>FreeBSD</title>

      <para>
        Geschwindigkeit:
        <screen>cdcontrol [-f <replaceable>Ger�t</replaceable>] speed <replaceable>[Geschwindigkeit]</replaceable></screen>
      </para>

      <para>
        DMA:
        <screen>sysctl hw.ata.atapi_dma=1</screen>
      </para>

    </sect2>

  </sect1>



  <sect1 id="dvd">
    <title>DVD-Wiedergabe</title>
    <para>
      F�r eine komplette Liste der verf�gbaren Optionen lies bitte die Manpage.
      Der Syntax f�r eine Standard Digital Versatile Disc (DVD) lautet wie folgt:
      <screen>mplayer dvd://<replaceable>&lt;Track&gt;</replaceable> [-dvd-device <replaceable>&lt;Ger�t&gt;</replaceable>]</screen>
    </para>

    <para>
      Beispiel:
      <screen>mplayer dvd://<replaceable>1</replaceable> -dvd-device <replaceable>/dev/hdc</replaceable></screen>
    </para>

    <para>
      Das Standard-DVD-Laufwerk ist <filename>/dev/dvd</filename>. Wenn deine
      Installation davon abweicht, erstelle einen Symlink oder gib das genaue Ger�t auf
      der Kommandozeile an mit der Option <option>-dvd-device</option>.
    </para>

      <para>
        <application>MPlayer</application> verwendet <systemitem>libdvdread</systemitem> und
        <systemitem>libdvdcss</systemitem> zur DVD-Entschu�sselung und -Wiedergabe.
        Diese beiden Bibliotheken sind im Unterverzeichnis
        <filename class="directory">libmpdvdkit2</filename> des
        <application>MPlayer</application>-Quelltextbaums.
        Du brauchst sie nicht separat zu installieren. Du kannst auch systemweite
        Versionen der beiden Bibliotheken verwenden, diese wird jedoch
        <emphasis role="bold">nicht</emphasis> empfohlen, da dies zu Bugs,
        Bibliotheksinkompatibilit�ten und geringerer Geschwindigkeit f�hren kann.
      </para>

    <note>
      <para>
        In F�llen von DVD-Dekodierungs-Problemen versuche Supermount oder solche Hilfen
        zu deaktivieren. Einige RPC-2 Laufwerke k�nnen verlangen, dass ein Regionalcode gesetzt ist.
      </para>
    </note>

    <formalpara>
      <title>DVD-Struktur</title>
      <para>
        DVDs haben 2048 Bytes pro Sektor mit ECC/CRC. Sie haben �blicherweise
        ein UDF-Dateisystem auf einem einzigem Track, welcher verschiedene Dateien
        (kleine .IFO und .BUK Dateien und gro�e .VOB Dateien) enth�lt.
        Sie sind echte Dateien und k�nnen von einem gemounteten Dateisystem einer
        unentschl�sselten DVD kopiert/abgespielt werden.
      </para>
    </formalpara>

    <para>
      Die .IFO-Dateien enthalten die Informationen zur Filmnavigation
      (Kapitel/Titel/Blickwinkel, Sprachtabellen etc.) und werden ben�tigt,
      um den .VOB Inhalt (den Film) zu lesen und zu interpretieren. Die .BUK-Dateien
      sind Backups davon. Sie nutzen �berall <emphasis role="bold">Sektoren</emphasis>,
      so dass du Direktaddressierung von Sektoren auf dem
      Datentr�ger ben�tigst, um DVD-Navigation zu implementieren oder den
      Inhalt zu entschl�sseln.
    </para>

    <para>
      DVD-Unterst�tzung ben�tigt rohen Sektor-basierten Zugriff auf das
      Laufwerk. Leider musst du (unter Linux) root sein, um die Sektoraddresse einer
      Datei zu erhalten. Das ist der Grund, warum wir nicht den Dateisystemtreiber
      des Kernels nutzen sondern es im Userspace reimplementiert haben.
      <systemitem>libdvdread</systemitem> 0.9.x und
      <systemitem>libmpdvdkit</systemitem> tun dies.
      Der UDF-Dateisystemtreiber des Kernels wird nicht ben�tigt, da sie bereits
      einen eigenen eingebauten UDF-Dateisystem-Treiber haben.
      Ebenso muss die DVD nicht gemountet werden, da der direkte Sektor-basierte Zugriff
      genutzt wird.
    </para>

    <para>
      Manchmal kann <filename>/dev/dvd</filename> nicht von Benutzern gelesen
      werden, deshalb implementierten die Autoren von <systemitem>libdvdread</systemitem>
      einen Emulations-Layer, welcher die Sektorenadressen in Dateinamen+Offsets
      �bertr�gt und Raw-Zugriff auf dem gemounteten Dateisystem oder auch
      auf Festplatten emuliert.
    </para>

    <para>
      <systemitem>libdvdread</systemitem> akzeptiert sogar Mountpoints an Stelle von
      Ger�tenamen f�r Raw-Zugriff und �berpr�ft
      <filename>/proc/mounts</filename>, um den Ger�tenamen herauszufinden.
      Es wurde f�r Solaris entwickelt, wo Ger�tenamen dynamisch
      zugewiesen werden.
    </para>

    <para>
      Wenn du <application>MPlayer</application> mit dvdnav-Unterst�tzung compiliert hast,
      bleibt der Syntax derselbe, ausgenommen die Verwendung von dvdnav:// an Stelle von dvd://.
    </para>

    <formalpara>
      <title>DVD-Authentifizierung</title>
      <para>
        Die Authentifizierung und Entschl�sslungsmethode der neuen DVD-Unterst�tzung
        wird von einer gepatchten <systemitem>libdvdcss</systemitem>-Bibliothek vorgenommen
        (siehe oben). Die Methode kann festgelegt werden durch die Umgebungsvariable
        <envar>DVDCSS_METHOD</envar>, welche auf key, disk oder title gesetzt werden
        kann.
      </para>
    </formalpara>

    <para>
      Wenn nichts angegeben wird, werden zuerst folgende Methoden probiert
      (Standard: key, title request):
    </para>

    <orderedlist>
      <listitem>
        <para>
          <emphasis role="bold">bus key</emphasis>: Dieser Schl�ssel
          ist ausgehandelt zwichen der Authentifizierung (ein langer Mix aus ioctls und
          verschiedenen Schl�sselaustauschen, Crypto-Zeug) und wird benutzt,
          um den Titel-Schl�ssel und den Datentr�ger-Schl�ssel
          vor dem Senden �ber den ungesch�tzten Bus
          (um Abh�ren zu verhinden) zu entschl�sseln.
          Der Busschl�ssel wird ben�tigt, um den verschl�sselten
          Datentr�ger-Schl�ssel zu erhalten und vorzuentschl�sseln.
        </para>
      </listitem>

      <listitem>
        <para>
          <emphasis role="bold">cached key</emphasis>: <application>MPlayer</application>
          sucht nach bereits geknackten Titel-Sch�sseln, welche im
          <filename class="directory">~/.mplayer/DVDKeys</filename> Verzeichnis
          gespeichert werden (schnell ;).
        </para>
      </listitem>

      <listitem>
        <para>
          <emphasis role="bold">key</emphasis>: Wenn kein zwichengespeicherter
          Schl�ssel verf�gbar ist, versucht <application>MPlayer</application>
          den Datentr�gerschl�ssel mittels einer Reihe enthaltener
          Player-Schl�ssel zu entschl�sseln.
        </para>
      </listitem>

      <listitem>
        <para>
          <emphasis role="bold">disk</emphasis>: Wenn die Schl�sselmethode fehlschl�gt
          (z.B. keine Player-Schl�ssel enthalten sind), wird <application>MPlayer</application>
          den Datentr�gerschl�ssel mittels eines Brute-Force-Algorithmus knacken.
          Dieser Prozess ist sehr CPU-lastig und ben�tigt 64 MB
          Arbeitsspeicher (16M 32Bit Hash-Tabelle), um die tempor�ren
          Daten zu speichern. Diese Methode sollte immer funktionieren (langsam).
        </para>
      </listitem>

      <listitem>
        <para>
          <emphasis role="bold">title request</emphasis>: Mit dem
          Datentr�gerschl�ssel fordert <application>MPlayer</application>
          die verschl�sselten Titelschl�ssel an, welche in
          <emphasis>versteckten Sektoren</emphasis> mittels <systemitem>ioctl()</systemitem>
          ausgelesen werden.
          Der Regionschutz von RPC-2-Laufwerken erfolgt an dieser Stelle und kann auf solchen
          Laufwerken fehlschlagen. Bei Erfolg werden die Titelschl�ssel mit dem Bus- und
          dem Datentr�gerschl�ssel entschl�sselt.
        </para>
      </listitem>

      <listitem>
        <para>
          <emphasis role="bold">title</emphasis>: Diese Methode wird genutzt, wenn die
          Titelanforderung fehlgeschlagen ist und keinen Schl�sselaustausch mit dem
          DVD-Laufwerk n�tig ist.
          Die Methode nutzt Crypto-Attacken, um den Titelschl�ssel direkt zu erraten
          (durch das Finden von wiederholenden Mustern im entschl�sselten VOB-Inhalt
          und dem Erraten, dass der zu den ersten entschl�sselten Bytes geh�rende
          Klartext eine Fortsetzung dieses Musters ist).
          Die Methode ist bekannt als &quot;known plaintext attack&quot;
          oder &quot;DeCSSPlus&quot;. In einigen wenigen F�llen kann dies
          fehlschlagen, da sich nicht genug verschl�sselte Daten auf dem
          Datentr�ger befinden, um eine statistische Attacke durchzuf�ren, oder
          weil der Schl�ssel sich mitten im Titel �ndert. Diese Methode ist der
          einzige Weg, eine DVD zu entschl�sseln, die auf einer Festplatte gespeichert
          ist oder auf einer DVD mit einer falschen Region auf einem
          RPC2-Laufwerk (langsam).
        </para>
      </listitem>
    </orderedlist>

    <para>
      <!-- Idee: (am Regioncode) hinzufuegen? -->
      <!-- Comment: Ich mag das Wort upgrade nicht. Oder besser
           gesagt sollte der Text besser auf die Risiken und Fakten eingehen. -->
      RPC-1 DVD-Laufwerke sch�tzen Regionseinstellunge nur durch Software.
      RPC-2-Laufwerke haben einen Hardwareschutz, welcher nur 5 �nderungen
      erlaubt. Es kann notwendig/empfehlenswert sein, die Firmware auf RPC-1
      zu aktualisieren, wenn du ein RPC-2 DVD-Laufwerk hast.
      Du kannst versuchen ein Firmwareupgrade f�r dein Laufwerk im Internet zu finden,
      <ulink url="http://forum.rpc-1.com/dl_all.php">dieses Firmware-Forum</ulink>
      kann ein guter Ausgangspunkt f�r deine Suche sein.
      Wenn es kein Firmwareupgrade f�r dein Laufwerk gibt, benutze das
      <ulink url="http://linvdr.org/projects/regionset/">regionset-Tool</ulink>,
      um den Regionscode deines DVD-Laufwerks (unter Linux) zu setzen.
      <emphasis role="bold">Warnung</emphasis>: Du kannst nur 5 mal den Regioncode �ndern.
    </para>
  </sect1>



  <sect1 id="vcd">
    <title>VCD-Wiedergabe</title>
    <para>
      F�r eine komplette Liste an verf�gbaren Optionen lies bitte die
      Manpage. Der Syntax f�r eine Standard Video CD (VCS) lautet wie folgt:
      <screen>mplayer vcd://<replaceable>&lt;Track&gt;</replaceable> [-cdrom-device <replaceable>&lt;Ger�t&gt;</replaceable>]</screen>
      Beispiel:
      <screen>mplayer vcd://<replaceable>2</replaceable> -cdrom-device <replaceable>/dev/hdc</replaceable></screen>
      Das Standard-VCD-Ger�t ist <filename>/dev/cdrom</filename>. Wenn deine
      Installation davon abweicht, erstelle einen Symlink oder gib das genaue Ger�t auf
      der Kommandozeile an mit der Option <option>-cdrom-device</option>.
    </para>

    <note>
      <para>
        <!-- TODO: have horrible performance reading VCD - zu ueberpruefen -->
        Mindenstens Plextor und einige SCSI-CD-ROM-Laufwerke von Toshiba haben eine
        schreckliche VCD-Leseleistung. Das liegt daran, da� der
        <systemitem>ioctl</systemitem> CDROMREADRAW f�r diese Laufwerke
        nicht vollstaendig implementiert ist. Wenn du ein bisschen Fachwissen �ber
        SCSI- Programmierung hast, <ulink url="../../tech/patches.txt">hilf uns</ulink>
        bitte, allgemeine SCSI-Unterst�tzggung f�r VCDs zu implementieren.
      </para>
    </note>

    <para>
      Inzwischen kannst du die Daten von VCDs mit
      <ulink url="http://ftp.ntut.edu.tw/ftp/OS/Linux/packages/X/viewers/readvcd/">readvcd</ulink>
      extrahieren und die ausgegebene Datei mit <application>MPlayer</application>
      abspielen.
    </para>

    <formalpara>
      <title>VCD-Struktur</title>
      <para>
        Eine Video CD (VCD) besteht aus CD-ROM XA Sektoren, z.B. CD-ROM Mode 2
        Form 1 und 2 Tracks:
      </para>
    </formalpara>

    <itemizedlist>
      <listitem>
        <para>
          Der erste Track ist im Format Mode 2 Form 2, was bedeutet, dass es
          L2-Fehlerkorrektur benutzt. Der Track enth�lt ein ISO-9660 Dateisystem
          mit 2048 Bytes/Sektor. Das Dateisystem enth�lt VCD Metadata-Informationen
          ebenso wie Standbilder, welche oft in Men�s benutzt werden.
          MPEG-Segmente f�r Men�s k�nnen auch im ersten Track gespeichert
          werden, die MPEGs m�ssen aber in eine Serie von 150-Sektoren-Einheiten
          gest�ckelt werden. Das ISO-9660 Dateisystem kann auch noch andere Dateien oder
          Programme enthalten, welche nicht f�r VCD-Betrieb erforderlich sind.
        </para>
      </listitem>

      <listitem>
        <para>
          Der zweite und die restlichen Tracks sind generelle rohe 2324 Bytes/Sektor
          MPEG (Film) Tracks, welche ein MPEG-PS-Datenpaket pro Sektor enthalten.
          Diese sind im Format Mode 2 Form 1, so dass sie mehr Daten pro Sektor speichern
          k�nnen unter dem Verlust einiger Fehlerkorrektur. Es ist ebenso
          g�ltig, CD-DA Tracks in einer VCD nach dem ersten Track zu haben.
          Auf manchen Betriebssystemen gibt es ein paar Tricks, diese nicht-ISO-9660-Tracks
          im Dateisystem erscheinen zu lassen. Auf anderen Systemen wie GNU/Linux ist
          dies (noch) nicht der Fall. Hier k�nnen die MPEG-Daten
          <emphasis role="bold">nicht gemountet werden</emphasis>. Da sich die meisten Filme
          in einer solchen Art von Track befinden, solltest du zuerst <option>vcd://2</option> versuchen.
        </para>
      </listitem>

      <listitem>
        <para>
          Es existieren ebenso VCDs ohne einen ersten Track (einzelner Track und
          �berhaupt kein Dateisystem). Diese sind trotzudem abspielbar,
          k�nnen aber nicht gemountet werden.
        </para>
      </listitem>

      <listitem>
        <para>
          Die Definition des Video-CD-Standards wird Philips "White Book" genannt und ist
          im Allgemeinen nicht online verf�gbar, da es von Philips k�uflich erworben werden
          muss. Detailliertere Informationen �ber Video-CDs befindet sich in der 
          <ulink url="http://www.vcdimager.org/pub/vcdimager/manuals/0.7/vcdimager.html#SEC4">vcdimager- Documentation</ulink>.
        </para>
      </listitem>

    </itemizedlist>

    <formalpara>
      <title>.DAT Dateien</title>
      <para>
        Die ~600 MB Datei, die auf dem ersten Track einer gemounteten VCD sichtbar
        ist, ist keine richtige Datei! Es ist ein sogenanntes ISO-Gateway, geschaffen,
        um Windows den Zugriff auf solche Tracks zu geben (Windows erlaubt �berhaupt
        keine direkten Zugriffe auf das Laufwerk). Unter Linux kannst du solche Dateien
        weder kopieren noch abspielen (sie enthalten M�ll). Unter Windows ist dies
        m�glich, da Windows ISO9660-Treiber das direkte Lesen der Tracks in diesen
        Dateien emuliert. Um eine .DAT Datei wiederzugeben, ben�tigst du einen
        Kernel-Treiber, welcher in der Linux-Version von PowerDVD zu finden ist.
        Es hat einen modifizierten ISO9660 Dateisystem-Treiber
        (<filename>vcdfs/isofs-2.4.X.o</filename>), welcher in der Lage ist,
        die rohen Tracks durch diese Schatten-.DAT-Dateien zu emulieren. Wenn du
        den Datentr�ger mit deren Treiber mountest, kannst du die .DAT Dateien
        kopieren und sogar mit <application>MPlayer</application> abspielen.
        Dies wird jedoch nicht mit dem Standard-ISO9660-Treiber des Linux-Kernels
        funktionieren! Benutze statt dessen <option>vcd://</option>. Alternativen
        f�r das Kopieren von VCDs sind der neue Kernel-Treiber
        <ulink url="http://www.elis.rug.ac.be/~ronsse/cdfs/">cdfs</ulink>
        (nicht Bestandteil des offiziellen Kernels), welcher CD-Sessions
        als Imagedateien darstellt, und
        <ulink url="http://cdrdao.sf.net/">cdrdao</ulink>,
        ein Bit-f�r-Bit CD-Grabbing/Kopier-Programm.
      </para>
    </formalpara>

  </sect1>

</chapter>
