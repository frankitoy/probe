<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- synced with r20379 -->
<chapter id="cd-dvd">
<title>Utilisation des CD/DVD</title>

<sect1 id="drives">
<title>Lecteurs CD/DVD</title>

<para>
Les lecteurs modernes de CD-ROM peuvent atteindre de tr�s hautes vitesses de lecture,
bien que certains soient capables de fonctionner � des vitesses r�duites.
Il y a plusieurs raisons possibles pour vouloir changer cette vitesse :
</para>

<itemizedlist>
<listitem><para>
Il a �t� signal� que des lecteurs peuvent commettre des erreurs de lecture �
haute vitesse, surtout avec des CD-ROM mal press�s. R�duire la vitesse peut alors
emp�cher la perte de donn�es dans ces circonstances.
</para></listitem>

<listitem><para>
Les lecteurs CD-ROM g�n�rent souvent un bruit assourdissant, qu'une vitesse r�duite
peut contribuer � diminuer.
</para></listitem>
</itemizedlist>

<sect2 id="drives_linux">
<title>Linux</title>

<para>
Vous pouvez r�duire la vitesse des lecteurs de CD-ROM IDE avec <command>hdparm</command>,
<command>setcd</command> ou <command>cdctl</command>. Ils fonctionnent comme suit�:
<screen>hdparm -E <replaceable>[vitesse]</replaceable> <replaceable>[p�riph. cdrom]</replaceable></screen>
<screen>setcd -x <replaceable>[vitesse]</replaceable> <replaceable>[p�riph. cdrom]</replaceable></screen>
<screen>cdctl -bS <replaceable>[vitesse]</replaceable></screen>
</para>

<para>
Si vous utilisez l'�mulation SCSI, vous pourriez avoir � appliquer les param�tres au
vrai p�riph�rique IDE, et non au p�riph�rique SCSI �mul�.
</para>

<para>
Si vous avez les privil�ges root, la commande suivante peut �galement aider�:
<screen>echo file_readahead:2000000 &gt; /proc/ide/<replaceable>[p�riph. cdrom]</replaceable>/settings</screen>
</para>

<para>
Ceci cr�� un cache de 2 Mo, ce qui est utile pour les CD-ROMs endommag�s (ray�s).
Si vous lui donnez une valeur trop haute, le lecteur ne va pas cesser de s'arr�ter
et de repartir, ce qui va dramatiquement diminuer les performances. Il est
�galement recommand� d'optimiser votre lecteur de CD-ROM avec <command>hdparm</command>�:
<screen>hdparm -d1 -a8 -u1 <replaceable>[p�riph. cdrom]</replaceable></screen>
</para>

<para>
Ceci permet l'acc�s DMA, le cache en lecture, et l'IRQ unmasking (lisez la page de
man de <command>hdparm</command> pour plus d'explications).
</para>

<para>
R�f�rez vous � "<filename>/proc/ide/<replaceable>[p�riph. cdrom]</replaceable>/settings</filename>"
pour optimiser pr�cis�mment votre lecteur CD-ROM.
</para>

<para>
Les lecteurs SCSI n'ont pas une mani�re uniforme de r�gler ces param�tres (Vous en
connaissez une ? Dites-la nous !). Il y a un outil qui fonctionne pour les
<ulink url="http://das.ist.org/~georg/">Lecteurs SCSI Plextor</ulink>.
</para>
</sect2>


<sect2 id="drives_freebsd">
<title>FreeBSD</title>

<para>Vitesse�:
<screen>
cdcontrol [-f <replaceable>p�riph�rique</replaceable>] speed <replaceable>[vitesse]</replaceable>
</screen>
</para>

<para>DMA�:
<screen>
sysctl hw.ata.atapi_dma=1
</screen>
</para>

</sect2>

</sect1>


<sect1 id="dvd">
<title>Lecture de DVD</title>
<para>
Pour voir la liste compl�te des options disponibles, veuillez lire la page de man.
La syntaxe pour lire un Digital Versatile Disc (DVD) est la suivante�:
<screen>mplayer dvd://<replaceable>&lt;piste&gt;</replaceable> [-dvd-device <replaceable>&lt;p�riph�rique&gt;</replaceable>]</screen>
</para>

<para>
Exemple�:
<screen>mplayer dvd://<replaceable>1</replaceable> -dvd-device <replaceable>/dev/hdc</replaceable></screen>
</para>

<para>
Si vous avez compil� <application>MPlayer</application> avec la gestion de dvdnav, la
syntaxe est la m�me, sauf que que vous devrez utiliser dvdnav:// au lieu de dvd://.
</para>

<para>
<application>MPlayer</application> utilise <systemitem>libdvdread</systemitem> et
<systemitem>libdvdcss</systemitem> pour le d�cryptage et la lecture de DVD.
Ces deux biblioth�ques sont contenues dans le sous-r�pertoire
<filename class="directory">libmpdvdkit2/</filename> du r�pertoire
source de <application>MPlayer</application>, vous n'avez donc pas besoin de les
installer s�par�ment. Vous pouvez aussi utiliser les versions de ces deux biblioth�ques
qui sont peut-�tre d�j� pr�sentes sur votre syst�me, mais cette solution n'est pas
recommand�e, dans la mesure o� elle peut provoquer des bogues, des incompatibilit�s
de biblioth�que et une vitesse r�duite.
</para>

<note><para>
En cas de probl�me de d�codage de DVD, essayez de d�sactiver supermount, ou
tous les outils de ce genre. Certains lecteurs RPC-2 peuvent aussi n�cessiter
le r�glage de leur code de r�gion.
</para></note>

<formalpara>
<title>Structure d'un DVD</title>
<para>
Les disques DVD utilisent tous des secteurs de 2048 octets par seconde avec ECC/CRC. Ils ont
g�n�ralement un syst�me de fichier UDF sur une seule piste, qui contient divers fichiers (des
petits fichiers .IFO et .BUK et de gros (1Go) fichiers .VOB). Ce sont de v�ritables fichiers et ils
peuvent �tre jou�s/copi�s depuis le syst�me de fichier mont� d'un DVD non-crypt�.
</para>
</formalpara>

<para>
Les fichiers .IFO contiennent les infos de navigation dans le film (carte des chapitres/titres/angles,
table des langues, etc) et sont requis pour lire et interpr�ter le contenu des .VOB (le film). Les
fichiers .BUK sont des backups de ces fichiers. Ils utilisent des <emphasis role="bold">secteurs</emphasis>
partout, donc vous avez besoin d'utiliser un adressage brut des secteurs sur le disque pour impl�menter
la navigation DVD. C'est �galement requis pour d�crypter le contenu.
</para>

<para>
Le support DVD n�cessite un acc�s brut aux secteurs du p�riph�rique. Malheureusement,
vous devez (sous Linux) �tre root pour avoir acc�s aux adresses des secteurs d'un fichier. C'est
pourquoi nous n'utilisons pas du tout le pilote de syst�me de fichier du noyau, �
la place nous l'avons r�impl�ment� en espace utilisateur. <systemitem>libdvdread</systemitem>
0.9.x et <systemitem>libmpdvdkit</systemitem> font cela. Le pilote pour le syst�me
de fichier UDF du noyau n'est pas requis puisqu'ils ont d�j� leur pilote UDF incorpor�.
De plus le DVD n'a pas besoin d'�tre mont� �tant donn� que seul l'acc�s brut aux secteurs est utilis�.
</para>

<para>
Parfois <filename>/dev/dvd</filename> ne peut �tre lu par les utilisateurs; les auteurs de
<systemitem>libdvdread</systemitem> ont donc impl�ment� une couche d'�mulation qui transforme les
adresses des secteurs en noms de fichier+offsets, pour �muler l'acc�s brut par-dessus un syst�me de fichier
mont� ou m�me un disque dur.
</para>

<para>
<systemitem>libdvdread</systemitem> accepte m�me le point de montage au lieu du nom de p�riph�rique
pour l'acc�s brut et regarde dans <filename>/proc/mounts</filename> pour obtenir le nom de p�riph�rique.
Il a �t� d�velopp� pour Solaris, o� les noms de p�riph�riques sont allou�s dynamiquement.
</para>

<para>
Le p�riph�rique DVD par d�faut est <filename>/dev/dvd</filename>. Si votre installation diff�re, faites
un lien symbolique ou sp�cifiez le bon p�riph�rique en ligne de commande avec l'option
<option>-dvd-device</option>.
</para>

<formalpara>
<title>Certification DVD</title>
<para>
La m�thode de certification et de d�cryptage pour le nouveau support des DVDs repose
sur l'utilisation d'un <systemitem>libdvdcss</systemitem> patch� (voir plus haut).
Cette m�thode peut �tre sp�cifi�e par l'interm�diaire de la variable d'environnement <envar>DVDCSS_METHOD</envar>
qui peut �tre r�gl�e sur key (clef), disk (disque) ou title (titre).
</para>
</formalpara>

<para>
Si rien n'est sp�cifi�, les m�thodes suivantes sont essay�es (par d�faut�: clef, demande de titre)�:
</para>

<orderedlist>
<listitem><para>
<emphasis role="bold">clef bus</emphasis>�: Cette clef est
n�goci�e durant l'authentification (une longue suite
d'ioctls et d'�changes de diverses clefs cryptographiques) et elle est
utilis�e pour crypter le titre et la clef du
disque avant de les envoyer sur un bus non prot�g� (pour
emp�cher les �coutes). La clef bus est n�cessaire
pour obtenir et pr�-d�crypter la clef disque crypt�e.
</para></listitem>

<listitem><para>
<emphasis role="bold">clef mise en cache</emphasis>�: <application>MPlayer</application>
cherche des titres de clef d�j� cass�e qui sont stock�es dans le r�pertoire
<filename class="directory">~/.mplayer/DVDKeys</filename> (rapide ;).
</para></listitem>

<listitem><para>
<emphasis role="bold">clef</emphasis>�: Si aucune clef n'est disponible dans le cache,
<application>MPlayer</application> essaye de d�crypter la clef disque avec un ensemble de clefs incluses.
</para></listitem>

<listitem><para>
<emphasis role="bold">disque</emphasis>�: Si la m�thode clef �choue (ex�: pas de clefs incluses),
<application>MPlayer</application> va casser la clef disque avec un algorithme de force brute. Ce
processus est intensif pour le CPU et requiert 60 Mo de m�moire (table de hachage 32Bit de 16M) pour
stocker temporairement les donn�es. Cette m�thode devrait toujours fonctionner (lent).
</para></listitem>

<listitem><para>
<emphasis role="bold">demande de titre</emphasis>�: Avec les clefs disque <application>MPlayer</application>
demande les clefs titre crypt�es, qui sont � l'int�rieur de <emphasis>secteurs cach�s</emphasis>
en utilisant <systemitem>ioctl()</systemitem>. La protection de r�gion des lecteurs RPC-2 devrait
op�rer � cette �tape et devrait �chouer sur ces lecteurs. Si elle r�ussit, les clefs titre seront
crypt�es avec les clefs bus et disque.
</para></listitem>

<listitem><para>
<emphasis role="bold">titre</emphasis>�: Cette m�thode est utilis�e si
la demande de titre a �chou� et ne repose sur aucun �change de clef avec
le lecteur DVD.
Il utilise une attaque crypto pour deviner la clef titre directement
(en trouvant un motif r�p�t� dans le contenu VOB d�cod� et en supposant
que le texte en clair correspondant aux premiers octets crypt�s est une
continuation de ce motif).
Cette m�thode est �galement connue sous le nom de "known plaintext attack"
ou "DeCSSPlus".
Dans de rares cas cela peut �chouer car il n'y a pas assez de donn�es crypt�es
sur le disque pour faire une attaque statistique ou � cause des changements de clef
au milieu du titre. C'est la seule fa�on de d�crypter un DVD stock� sur le disque dur
ou un DVD avec une mauvaise r�gion sur un lecteur RPC2(lent).
</para></listitem>
</orderedlist>

<para>
Les lecteurs DVD RPC-1 ne prot�gent les r�glages de protection de r�gion
que de fa�on logicielle.
Les lecteurs RPC-2 ont une protection mat�rielle qui ne permet que 5 changements.
Il peut �tre requis/recommand� de mettre � niveau le firmware en RPC-1
si vous avez un lecteur DVD RPC-2.
Les mises � niveau firmware peuvent �tre trouv�es sur cette
<ulink url="http://www.firmware-flash.com">page de firmware</ulink>.
Si il n'y a pas de mise � niveau de firmware disponible pour votre p�riph�rique,
utilisez l'<ulink url="http://www.linuxtv.org/download/dvd/dvd_disc_20000215.tar.gz">outil
de r�glage de r�gion</ulink> pour changer le code de r�gion de votre lecteur DVD (sous Linux).
<emphasis role="bold">Attention</emphasis>�:
Vous ne pouvez changer la r�gion que 5 fois.
</para>
</sect1>



<sect1 id="vcd">
<title>Lecture de VCDs</title>
<para>
Pour voir la liste compl�te des options disponibles, veuillez lire la page de man.
La syntaxe pour lire un Video CD standard (VCD) est la suivante�:
<screen>mplayer vcd://<replaceable>&lt;piste&gt;</replaceable> [-cdrom-device <replaceable>&lt;p�riph�rique&gt;</replaceable>]</screen>
Exemple�:
<screen>mplayer vcd://<replaceable>2</replaceable> -cdrom-device <replaceable>/dev/hdc</replaceable></screen>
Le p�riph�rique VCD par d�faut est <filename>/dev/cdrom</filename>. Si votre installation diff�re,
faites un lien symbolique ou sp�cifiez le bon p�riph�rique en ligne de commande avec l'option
<option>-cdrom-device</option>.
</para>

<note><para>
Les CD-ROM SCSI Plextor et certains Toshiba, entre autres, ont d'horribles
performances durant la lecture de VCDs.
C'est parce que l'<systemitem>ioctl</systemitem> CDROMREADRAW n'est pas
compl�tement impl�ment� pour ces lecteurs.
Si vous avez des connaissances en programmation SCSI, merci de
<ulink url="../../tech/patches.txt">nous aider</ulink> � impl�menter un
support SCSI g�n�rique pour les VCDs.
</para></note>

<para>
En attendant vous pouvez extraire des donn�es d'un VCD avec
<ulink url="http://ftp.ntut.edu.tw/ftp/OS/Linux/packages/X/viewers/readvcd/">readvcd</ulink>
et lire le fichier obtenu avec <application>MPlayer</application>.
</para>

<formalpara>
<title>Structure d'un VCD</title>

<para>
Un CD Video (VCD) est constitu� de secteurs CD-ROM XA, c'est-�-dire CD-ROM mode 2
forme 1 et 2 pistes�:</para>
</formalpara>

<itemizedlist>
<listitem><para>
La premi�re piste est en mode 2 forme 2 ce qui signifie qu'elle utilise une
correction d'erreur L2. La piste contient un syst�me de fichiers ISO-9660 avec 2048
octets/secteur. Ce syst�me de fichiers contient des informations VCD meta-donn�e, aussi
bien que les images fixes souvent utilis�es dans les menus. Les segments MPEG pour les menus
peuvent aussi �tre stock�s dans la premi�re piste, mais les donn�es MPEG doivent �tre cass�es
en s�ries de bouts de 150 secteurs. Le syst�me de fichiers ISO-9660 peut contenir d'autres
fichiers ou programmes qui ne sont pas essentiels pour les
op�rations VCD.
</para></listitem>

<listitem><para>
La seconde piste et les suivantes sont des pistes MPEG brutes (film) � 2324 octets/secteur,
contenant un paquet de donn�es MPEG PS par secteur. Celles-ci sont format�es selon le mode 2 forme 1,
elles stockent donc plus de donn�es par secteur au d�triment de la correction d'erreur.
Il est aussi permis d'avoir des pistes CD-DA dans un VCD apr�s la premi�re piste.
Sur certains syst�mes d'exploitation, il y a quelques astuces qui permettent de faire
appara�tre ces pistes non-ISO-9660 dans un syst�me de fichiers. Sur d'autres syst�mes
d'exploitation comme GNU/Linux cela n'est pas le cas (pas encore).
Ici les donn�es MPEG <emphasis role="bold">ne peuvent �tre mont�es</emphasis>.
Comme la plupart des films sont � l'int�rieur de ce genre de piste, vous devrez
tout d'abord essayer <option>vcd://2</option>.
</para></listitem>

<listitem><para>
Il existe �galement certains disques VCD sans la premi�re piste (une seule piste et pas de syst�me de
fichier du tout). Ils sont quand m�me lisibles, mais ne peuvent pas �tre mont�s.
</para></listitem>

<listitem><para>La d�finition du standard Video CD est appel�e le
"Livre Blanc" Philips et n'est g�n�ralement pas disponible en ligne, �tant donn�
qu'elle doit �tre achet�e aupr�s de Philips. Une information plus d�taill�e sur le Video
CD peut �tre trouv�e dans la
<ulink url="http://www.vcdimager.org/pub/vcdimager/manuals/0.7/vcdimager.html#SEC4">documentation de vcdimager</ulink>.
</para></listitem>

</itemizedlist>

<formalpara>
<title>� propos des fichiers .DAT�:</title>
<para>
Le fichier de ~600 Mo visible sur la premi�re piste d'un VCD mont� n'est
pas un vrai fichier�!
C'est ce qu'on appelle une passerelle iso, cr��e pour permettre � Windows
de g�rer de telles pistes (Windows n'autorise pas l'acc�s brut au
p�riph�rique du tout).
Sous linux, vous ne pouvez pas copier ou lire de telles pistes (elle
contiennent des informations parasites).
Sous Windows c'est possible car son pilote iso9660 �mule la lecture brute
des pistes dans ce fichier.
Pour lire un fichier .DAT vous avez besoin d'un pilote noyau qui peut
�tre trouv� dans la version Linux de <application>PowerDVD</application>.
Il poss�de un pilote de syst�me de fichier iso9660 modifi�
(<filename>vcdfs/isofs-2.4.X.o</filename>), qui est capable d'�muler
les pistes brutes au travers de ce fichier .DAT fant�me.
Si vous montez le disque en utilisant leur pilote, vous pouvez
copier et m�me lire les fichiers .DAT avec <application>MPlayer</application>.
Mais cela ne <emphasis role="bold">fonctionnera pas</emphasis>
avec le pilote iso9660 standard du noyau�!
Il est recommand� d'utiliser l'option <option>vcd://</option>
� la place.
D'autres possibilit�s pour la copie de VCD sont le nouveau pilote noyau
<ulink url="http://www.elis.rug.ac.be/~ronsse/cdfs/">cdfs</ulink>
(qui ne fait pas partie du noyau officiel) qui montre les sessions du CD
en temps que fichier image et
<ulink url="http://cdrdao.sf.net/">cdrdao</ulink>, une application
d'enregistrement/copie bit-�-bit).
</para>
</formalpara>
</sect1>
</chapter>
