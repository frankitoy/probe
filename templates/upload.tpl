

{if $secondpage ne "second" and $smarty.request.upload_final eq ""}
<table>
<tr>
<td height="24" valign=top align=center><table width="760"  border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td width="5" height="24" background="images/nv_2_l.jpg" valign=top><img src="images/nv_2_l.jpg" width="5" height="24" alt=""></td>
                                    <th background="images/nv_2_bg.jpg" align=left width=760><span class="white_bold" valign=top align=left>Video Upload (Step 1 of 2)</span></th>
                                    <td width="5" background="images/nv_2_r.jpg"><img src="images/nv_2_r.jpg" width="5" height="24" alt=""></td>
                                  </tr>
                              </table></td>

</tr></table>

<FORM id=theForm name=theForm action="{$baseurl}/upload.php" method=post encType=multipart/form-data>
<TABLE cellSpacing=0 cellPadding=5 width="100%" border=0><TBODY>
<TR>
<TD align=right width=200><SPAN class=label><NOBR>Title:</NOBR></SPAN></TD>
<TD><INPUT maxLength=60 size=40 name="field_myvideo_title" value="{$smarty.request.field_myvideo_title}"></TD>
</TR>
<TR>
<TD vAlign=top align=right><SPAN class=label><NOBR>Description:</NOBR> </SPAN></TD>
<TD><TEXTAREA name="field_myvideo_descr" rows=4 cols=50>{$smarty.request.field_myvideo_descr}</TEXTAREA></TD>
</TR>
<TR vAlign=top>
<TD align=right><SPAN class=label><NOBR>Tags:</NOBR> </SPAN></TD>
<TD><INPUT maxLength=120 size=40 name="field_myvideo_keywords" value="{$smarty.request.field_myvideo_keywords}">
<DIV class=formFieldInfo><STRONG>Enter one or more tags, separated
by spaces.</STRONG> <BR>Tags are simply keywords used to describe
your video so they are easily searched and organized. For example,
if you have a surfing video, you might tag it: surfing beach
waves.<BR></DIV></TD>
</TR>
<TR vAlign=top>
<TD align=right><SPAN class=label><NOBR>Video Channels:</NOBR></SPAN></TD>
<TD>
        <TABLE><TBODY><TR><TD vAlign=top>
        {insert name=list_channel assign=chinfo vid=$VID}
        {section name=i loop=$chinfo}
        <INPUT type=checkbox name=chlist[] value={$chinfo[i].id}>{$chinfo[i].ch}<BR>
        {/section}
        </TD></TR></TBODY></TABLE>

<DIV class=formFieldInfo><STRONG>Select between one to three
channels that best describe your video.</STRONG><BR>It helps to use
relevant channels so that others can find your video!</DIV>
</TD>
</TR>
<TR>
<TD align=right>&nbsp;</TD>
<TD><INPUT type=submit value="Continue ->" name=action_upload></TD></TR>
</TBODY></TABLE>
</FORM>

{else}
<table align="center">
<tr>
    <td height="24" valign=top align=center>
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
	<td nowrap class="activeTab">
	    <div class="active1">
		<div class="active2">
		    <div class="active3">
		    </div>
		</div>
	    </div>
	    <table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="5" height="24" bgcolor="#8CC63E" valign=middle></td>
                <th class=white_bold width=730 align=left valign="middle" bgcolor="#8CC63E">Video Upload (Step 2 of 2)</th>
                <td width="5" bgcolor="#8CC63E"></td>
            </tr>
    	    </table>
	</td>
    </tr>
    </table>
    </td>
</tr>
</table>

<table width="700" border="0" align="center" cellpadding="5" cellspacing="0">
<form name="form_upload" method="post" enctype="multipart/form-data" action="{$path_to_upload_script}" style="margin: 0px; padding: 0px;">
<tr>
    <td width="100" align="right" valign="top"><span class="label">File:</span></td>
    <td width="500" valign="top">
	<div class="formHighlight">
	    <input type="hidden" name="MAX_FILE_SIZE" value="104857600">
	    <input type="hidden" name="upload_range" value="1">
	    <noscript>
		<input type="hidden" name="no_script" value="1">
	    </noscript>
	    <input type="hidden" name="adult" value="{$adult}">
    	    <input type="hidden" name="field_myvideo_keywords" value="{$smarty.request.field_myvideo_keywords}">
    	    <input type="hidden" name="field_myvideo_title" value="{$smarty.request.field_myvideo_title}">
    	    <input type="hidden" name="field_myvideo_descr" value="{$smarty.request.field_myvideo_descr}">
    	    <input type="hidden" name="listch" value="{$listch}">
	    <div id="upload_slots">
		<input type="file" name="upfile_0" size="40" {if $multi_upload_slots eq '1'}onChange="addUploadSlot(1)"{/if} value="">
	    </div>
	    <div class="formHighlightText">
		<strong>Max file size: 100 MB. No copyrighted or obscene material.</strong>
		<br>After uploading, you can edit or remove this video at anytime under the "My Videos" link on the top of the page.<br>
	    </div>
	</div>
    </td>
</tr>
<tr>
    <td align="right"><span class="label">Broadcast:</span></td>
    <td>
	<table cellpadding="1" cellspacing="0" border="0">
	<tr>
	    <td><input name="field_privacy" type="radio" value="public" checked></td>
	    <td>
		<span class="black_regular1"><strong>Public:</strong></span>
		<span class="black_regular1">Share your video with the world! (Recommended)</span>
	    </td>
	</tr>
	</table>
    </td>
</tr>
<tr>
    <td align="right"><span class="label">&nbsp;</span></td>
    <td>
	<table cellpadding="1" cellspacing="0" border="0">
	<tr>
	    <td><input name="field_privacy" type="radio" value="private"></td>
	    <td>
		<span class="black_regular1"><strong>Private:</strong></span>
		<span class="black_regular1"> Only viewable by you and those you share the video with</span>.</span>
	    </td>
	</tr>
	</table>
    </td>
</tr>
</form>
<tr>
    <td width="100" align="right"><span class="label">Progress:</span></td>
    <td width="500">
	<!-- Start Progress Bar -->
	<div class="info" id="progress_info"></div>
	<div id="progress_bar" style="display:none">
	    <div class="bar1" id="upload_status_wrap" align="center">
		<div class="bar2" id="upload_status"></div>
	    </div>
	    <br>
	    <table class="data" cellpadding='3' cellspacing='1'>
	    <tr>
		<td align="left"><b>Percent Complete:</b></td>
		<td align="center"><span id="percent">0%</span></td>
	    </tr>
	    <tr>
		<td><b>Files Uploaded:</b></td>
		<td align="center"><span id="uploaded_files">0</span> of <span id="total_uploads"></span></td>
	    </tr>
	    <tr>
		<td align="left"><b>Current Position:</b></td>
		<td align="center"><span id="current">0</span> / <span id="total_kbytes"></span> KBytes</td>
	    </tr>
	    <tr>
		<td align="left"><b>Elapsed time:</b></td>
		<td align="center"><span id="time">0</span></td>
	    </tr>
	    <tr>
		<td align="left"><b>Est Time Left:</b></td>
		<td align="center"><span id="remain">0</span></td>
	    </tr>
	    <tr>
		<td align="left"><b>Est Speed:</b></td>
		<td align="center"><span id="speed">0</span> KB/s.</td>
	    </tr>
	    </table>
	</div>
	<!-- End Progress Bar -->
    </td>
</tr>
<tr>
    <td align="right">&nbsp;</td>
    <td>
	<noscript>
	<br>
	    <input type="reset" name="no_script_reset" value="Reset">&nbsp;&nbsp;&nbsp;<input type="submit" name="no_script_submit" value="Upload">
	</noscript>
	<br>
	<script language="javascript" type="text/javascript">
	<!--
	    document.writeln('<input type="button" name="reset_button" value="Reset" onClick="resetForm();">&nbsp;&nbsp;&nbsp;<input type="button" id="upload_button" name="upload_button" value="Upload" onClick="uploadFiles();">');
	//-->
	</script>
    </td>
</tr>
</table>
{/if}
