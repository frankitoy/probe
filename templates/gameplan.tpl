<table width="100%">
  <tr>
    <td align="center">
	<p>&nbsp;</p>
	<table width="960" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="360" align="left" valign="top"><h3>About Gameplan </h3>
      <p>In 1995, the sports scene on Philippine television was lackluster,  dull, and well&hellip; ho-hum. So the creative minds at Probe Productions,  Inc. put together a magazine show that would stimulate, motivate and  inspire, like a serum of adrenalin on a weekly basis.</p>
      <p align="left"> Enter  GamePlan. A revolutionary sports magazine show that set the standard for local  sports journalism.</p>
      <p align="left"> The show has taken audiences through every kind of terrain  imaginable&mdash;on land, in the air, over and under water. Thanks to a  talented team who feel most comfortable living on the edge, you get a  front row seat to adventures like ziplining in Cagayan de Oro, white  water rafting in Kalinga and downhill biking on the slopes of Tagaytay.</p>
      <p align="left"> Since the airing of its pilot episode on July 9, 1995, GamePlan has  continued to experiment and innovate&mdash;always challenging the limits of  sports programming. Because of its trailblazing attitude, the show is  still fresh and exciting even after 11 years on the air. </p>
      <p align="left">GamePlan is  serious about  the subject matter it knows best. Unsullied. Untainted. Unadulterated.  No fancy camera tricks. Just good old story telling. Because we believe  that a good sports story should stand alone. The way a champion always  stands alone.&nbsp;</p>
      <p align="left">We&rsquo;ve  set the standard. We keep raising the  bar. That&rsquo;s always been our gameplan. </p>
      <p align="left">&nbsp;</p></td>
    <td width="40" align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top"><h3>Hosts</h3>
      <p>Yas Mapua (1995)<br />
        Rufi Parpan (1995)
        <br />
        Paolo Abrera (1995)<br />
        Mon Castro <br />
        Paolo Bediones
        <br />
        Suzi Entrata<br />
        Carlo Ledesma (2001) <br />
        Tricia Chiongbian (2001) <br />
        Rovilson Fernandez (2001) <br />
        Akiko Thomson (2001) <br />
        Paolo Soler (2005) <br />
        Wowie Meloto (2005) </p>
      <p>&nbsp; </p></td>
    <td width="20" align="left" valign="top">&nbsp;</td>
    <td width="332" align="left" valign="top">
	  <h3>Featured Video 	    </h3>
	  <p>
	    <object type="application/x-shockwave-flash" wmode="transparent" data="http://www.probetv.com/flvplayer.swf?file=http://www.probetv.com/flvideo/430.flv&autostart=false&amp;showfsbutton=false" height="260" width="320">
	      <br />  
	      <param name="movie" value="http://www.probetv.com/flvplayer.swf?file=http://www.probetv.com/flvideo/430.flv&autostart=false&amp;showfsbutton=false">
	      <br />  
	      <param name="wmode" value="transparent">
	      <br />  
	      <param name="allowScriptAccess" value="sameDomain">
	      <br />  
	      <embed src="http://www.probetv.com/flvplayer.swf?file=http://www.probetv.com/flvideo/430.flv&autostart=true&amp;showfsbutton=true" loop="false" allowscriptaccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" height="260" width="320"></embed>
                </object>
	  </p>
	  <p>Gameplan	Farewell Episode - part 1<br />
	    aired on RPN 9 on June 30, 2007</p>
	  <p>> <a href="http://www.probetv.com/channel_detail.php?chid=3">Watch Gameplan episodes</a> </p></td>
  </tr>
</table>

	
	</td>
  </tr>
</table>
