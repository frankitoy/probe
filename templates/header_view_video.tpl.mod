<html>
<head>
<title>{$site_name} - {$answers[0].description}</title> 
<meta name="title" content="{$answers[0].description}" /> 
<meta name="abstract" content="{$answers[0].description}" /> 
<meta name="keywords" content="{section name=i loop=$tags}{$tags[i]}, {/section}" /> 
<meta name="description" content="{$answers[0].description}" /> 


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="alternate" type="application/rss+xml" title="RSS - 20 newest videos" href="{$baseurl}/rss/new/" /> 
<link rel="alternate" type="application/rss+xml" title="RSS - 20 most viewed videos" href="{$baseurl}/rss/views/" /> 
<link rel="alternate" type="application/rss+xml" title="RSS - 20 most commented videos" href="{$baseurl}/rss/comments/" /> 


	 <!--!!!!!!!!!!!!!!!!!!!!!!!! LIBRARY !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!-->
	<script src="{$baseurl}/ajax/cpaint2.inc.js" type="text/javascript"></script>
	<script src="{$baseurl}/js/myjavascriptfx.js" type="text/javascript"></script>
	<!-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->

	<!--!!!!!!!!!!!!!!!!!!!!!!!!! Processing SCRIPT !!!!!!!!!!!!!!!!!!!-->
	<script language=JavaScript src={$baseurl}/js/indexonly.js></script>
	<script language=JavaScript src={$baseurl}/js/myjavascriptajax.js></script>
	<script language=JavaScript src={$baseurl}/js/myjavascript.js></script>
	<script language=JavaScript src={$baseurl}/js/player_fs.js></script>
	<!-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->

	<!--!!!!!!!!!!!!!!!!!!!!!!!!! CSS !!!!!!!!!!!!!!!!!!!-->
	<link href="{$baseurl}/css/style.css" rel="stylesheet" type="text/css">
	<link href="{$baseurl}/modules/bookmarks/addto.css" rel="stylesheet" type="text/css">	
	

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" background-color="#F2F2F2" >
<br>
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>

        <td><table width="778" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    
                    <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                          <tr>


                            <TD vAlign=top>
                                 <TABLE cellSpacing=0 cellPadding=0 width=750 border=0>
                                <TBODY>
                                        <TR vAlign=center>
						<td height="50" align="right" width=212>
							<div align="center"><img src="images/logo4.gif"   alt="">						</div></td>
                                                <td height="40"><span class="style1">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="deep_gray_ragular">Making a positive contribution to Philippine television </span></td>
                                                <TD align=center><span class="normal_blue">
                                                
                                                        {insert name="msg_count" assign=total_msg}
                                                        {if $smarty.session.USERNAME ne ""}
                                                                <TABLE cellSpacing=0 cellPadding=0 border=0><TBODY><TR>
                                                                <TD>
                                                                        <A href="{$baseurl}/my_profile.php">{$smarty.session.USERNAME}</A>!
                                                                                &nbsp; <A href="{$baseurl}/inbox.php"><IMG
                                                                                style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 1px; VERTICAL-ALIGN: text-bottom; PADDING-TOP: 1px"
                                                                                height=12
                                                                                {if $total_msg eq ""}
                                                                                src="{$imgurl}/icon_mail_off.gif" width=14 border=0>{else}
                                                                                src="{$imgurl}/newmail.gif" border=0>{/if}</A>
                                                                        (<A href="{$baseurl}/inbox.php">{$total_msg}</A>) </TD>
                                                                <TD style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px">|</TD>
                                                                <TD><A href="{$baseurl}/logout.php">Log Out</A></TD>
                                                                <TD style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px">|</TD>
                                                                <TD style="PADDING-RIGHT: 5px"><A
                                                                href="{$baseurl}/help.php">Help</A></TD></TR></TBODY></TABLE></TD></TR></TBODY>
                                                                </TABLE>

                                                        {else}

                                                                <TABLE cellSpacing=0 cellPadding=0 border=0>
                                                                <TBODY>
                                                                        <TR>
										
                                                                                <TD>
											<A href="{$baseurl}/signup.php"><STRONG>Sign Up</STRONG></A>
										</TD>
                                                                                <TD
                                                                                        style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px">|
										</TD>
                                                                                <TD>
											<A href="{$baseurl}/login.php">Log In</A></TD>
                                                                                <TD
                                                                                        style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px">|</TD>
                                                                                        <TD style="PADDING-RIGHT: 5px"><A
                                                                                                href="{$baseurl}/help.php">Help</A></TD></TR></TBODY></TABLE></TD></TR></TBODY>
                                                                </TABLE>
                                                        {/if}
						</span>
						</td>
					</tr>
				</tbody>
                        </table>
			
			</td>
                      </tr>
                      <tr>
                        <td align="center">
			<table width="750"  border="0" cellspacing="0" cellpadding="0">
                          <tr align="center">
												
                            <td width="125" height="26" background="images/button.jpg"> <span class="navigation_bold"><a href="{$baseurl}/index.php">Home</a></span></td>
                            <td width="125" background="images/button.jpg" class="navigation_bold"> <a href="{$baseurl}/upload.php">Upload</a></td>
			    <td width="125" background="images/button.jpg" class="navigation_bold"> <a href="{$baseurl}/video.php?next=watch">Videos</a></td>
                            <td width="125" background="images/button.jpg" class="navigation_bold"> <a href="{$baseurl}/channels.php">Channels</a></td>
                            <td width="125" background="images/button.jpg" class="navigation_bold"> <a href="{$baseurl}/groups.php">Groups</a></td>
                            <td width="125" background="images/button.jpg" class="navigation_bold"> <a href="{$baseurl}/friends.php">Friends</a></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table></td>
                  </tr>
                </table></td>
              </tr>
              
              
{if $head_bottom ne ""}
{include file = head_bottom/$head_bottom}
{else}
{include file = head_bottom/blank.tpl}
{/if}
{if $head_bottom_add ne ""}
{include file = head_bottom/$head_bottom_add}
{/if}
