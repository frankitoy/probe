<html>
<head>
<title>{$site_name}</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
{if $upload_page eq 'upload'}
    <meta name="robots" content="index,nofollow">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
{/if}

<link rel="alternate" type="application/rss+xml" title="RSS - 20 newest videos" href="{$baseurl}/rss/new/" /> 
<link rel="alternate" type="application/rss+xml" title="RSS - 20 most viewed videos" href="{$baseurl}/rss/views/" /> 
<link rel="alternate" type="application/rss+xml" title="RSS - 20 most commented videos" href="{$baseurl}/rss/comments/" /> 



	 <!--!!!!!!!!!!!!!!!!!!!!!!!! LIBRARY !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!-->
	<script src="{$baseurl}/ajax/cpaint2.inc.js" type="text/javascript"></script>
	<script src="{$baseurl}/js/myjavascriptfx.js" type="text/javascript"></script>
	<!-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->

	<!--!!!!!!!!!!!!!!!!!!!!!!!!! Processing SCRIPT !!!!!!!!!!!!!!!!!!!-->
	<script language=JavaScript src={$baseurl}/js/indexonly.js></script>
	<script language=JavaScript src={$baseurl}/js/myjavascriptajax.js></script>
	<script language=JavaScript src={$baseurl}/js/myjavascript.js></script>
	<link href="{$baseurl}/css/style.css" rel="stylesheet" type="text/css">
	<link href="{$baseurl}/css/upload-progress.css" rel="stylesheet" type="text/css">
	<!-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
{if $upload_page eq 'upload'}
    <script language="javascript" type="text/javascript">
	var tmp_sid = "{$tmp_sid}";
	var disallow_extensions = {$disallow_extensions}
	var allow_extensions = {$allow_extensions};
	var path_to_ini_status_script = "{$path_to_ini_status_script}";
	var check_file_name_format = {$check_file_name_format};
	var check_disallow_extensions = {$check_disallow_extensions};
	var check_allow_extensions = {$check_allow_extensions};
	var check_null_file_count = {$check_null_file_count};
	var check_duplicate_file_count = {$check_duplicate_file_count};
	var max_upload_slots = {$max_upload_slots};
	var progress_bar_width = {$progress_bar_width};
    </script>
    <script language="javascript" type="text/javascript" src="{$baseurl}/js/uu_file_upload.js"></script>
{/if}
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" background-color="#F2F2F2" {if $upload_page eq 'upload'}onLoad="iniFilePage()"{/if}>

<!-- New header by RMS -->
<table width="960" height=82 align="center" border="0" cellspacing="0" cellpadding="0" background="../images/header_bg.gif" style="background-repeat:no-repeat;">
  <tr>
    <td width="190" rowspan="2">
	  <a href="http://www.probetv.com"><img src="../images/logo5.gif" hspace="20" border="0"></a> </td>
    <td width="400" height="46">&nbsp;</td>
    <td width="370" height="46">
	<!-- Login links -->
	
	
	
	
	
	</td>
  </tr>
  <tr>
    <td height="36" valign="top">
	<script language="javascript" src="../menu/menu1.js"></script></td>
    <td height="36" valign="middle">
	<!-- search form -->
	
	<form name=searchForm action="{$baseurl}/search_result.php" method="get" >
    <table width="320" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="">
	      <input class="header_input" size=30 tabIndex=1 maxLength=128 name="search_id" value="{$smarty.request.search_id}">	</td>
        <td width="">
	      <select name=search_typ class=header_select>
    {if $smarty.request.search_type eq "search_videos"}<OPTION value=search_videos selected>Search Videos</OPTION>{else}<OPTION value=search_videos>Videos</OPTION>{/if}
    {if $smarty.request.search_type eq "search_users"}<OPTION value=search_users selected>Search Users</OPTION>{else}<OPTION value=search_users>Users</OPTION>{/if}
    {if $smarty.request.search_type eq "search_groups"}<OPTION value=search_groups selected>Search Groups</OPTION>{else}<OPTION value=search_groups>Groups</OPTION>{/if}
    </select>	</td>
        <td width="" align="left">
          <input type="image" src="../images/search_button.gif" height="21" width="56">	</td>
        <td width="10" align="left">&nbsp;</td>
      </tr>
    </table>
    </form>
	</td>
  </tr>
</table>





<table width="964" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td><table width="962" border="0" cellspacing="0" cellpadding="0">
      <tr>

        <td><table width="962" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    
                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>


                            <TD vAlign=top>
                                 <TABLE cellSpacing=0 cellPadding=0 width=100% border=0 background="">
                                <TBODY>
                                        <TR vAlign=center>
						<td align="left" width=180>&nbsp;</td>
                                                <td valign="bottom">
								
<!-- Old search form
<FORM name=searchForm action="{$baseurl}/search_result.php" method="get">
 <td align="center">
      <table width="300"  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                                <td>
                                        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                                <td width="100">&nbsp;</td>
                                                <td height="34">
                                                        <INPUT size=20 tabIndex=1 maxLength=128 name="search_id" value="{$smarty.request.search_id}">
                                                </td>
                                                <td><SELECT name=search_typ class=select_back_white>
                                                        {if $smarty.request.search_type eq "search_videos"}<OPTION value=search_videos selected>Search Videos</OPTION>{else}<OPTION value=search_videos>Search Videos</OPTION>{/if}
                                                        {if $smarty.request.search_type eq "search_users"}<OPTION value=search_users selected>Search Users</OPTION>{else}<OPTION value=search_users>Search Users</OPTION>{/if}
                                                        {if $smarty.request.search_type eq "search_groups"}<OPTION value=search_groups selected>Search Groups</OPTION>{else}<OPTION value=search_groups>Search Groups</OPTION>{/if}
                                                    </SELECT>
                                                </td>
                                        <td><input type=image src="images/search_b.jpg" width="90" height="20"></td>
                                        <td width="100">&nbsp;</td> 
                                        </tr>
                              </table>
                            </td>
                        </tr>
                    </table>

                    </td>
        </FORM>
		-->					  </td>
                                                <TD align=center><span class="normal_blue">
<!-- Login links                                                
{insert name="msg_count" assign=total_msg}
{if $smarty.session.USERNAME ne ""}
<TABLE cellSpacing=0 cellPadding=0 border=0>
  <TBODY>
    <TR>
      <TD>
        <A href="{$baseurl}/my_profile.php">{$smarty.session.USERNAME}</A>!&nbsp; <A href="{$baseurl}/inbox.php"><IMG style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 1px; VERTICAL-ALIGN: text-bottom; PADDING-TOP: 1px" height=12
                                                                                {if $total_msg eq ""}
                                                                                src="{$imgurl}/icon_mail_off.gif" width=14 border=0>{else}
                                                                                src="{$imgurl}/newmail.gif" border=0>{/if}</A>

        (<A href="{$baseurl}/inbox.php">{$total_msg}</A>) </TD>
                                                                <TD style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px">|</TD>
                                                                <TD><A href="{$baseurl}/logout.php">Log Out</A></TD>
                                                                <TD style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px">|</TD>
                                                                <TD style="PADDING-RIGHT: 5px"><A
                                                                href="{$baseurl}/help.php">Help</A></TD></TR></TBODY></TABLE></TD></TR></TBODY>
                            </TABLE>

                                                        {else}

                                                                <TABLE cellSpacing=0 cellPadding=0 border=0>
                                                                <TBODY>
                                                                        <TR>
										
                                                                                <TD>
											<A href="{$baseurl}/signup.php"><STRONG>Sign Up</STRONG></A>
										</TD>
                                                                                <TD
                                                                                        style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px">|
										</TD>
                                                                                <TD>
											<A href="{$baseurl}/login.php">Log In</A></TD>
                                                                                <TD
                                                                                        style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px">|</TD>
                                                                                        <TD style="PADDING-RIGHT: 5px"><A
                                                                                                href="{$baseurl}/help.php">Help</A></TD></TR></TBODY></TABLE> -->
																								
										  </TD></TR></TBODY>
                                                                </TABLE>
                                                        {/if}
						</span>
						</td>
					</tr>
				</tbody>
                        </table>
			
			</td>
                      </tr>
                      <tr>

                      </tr>
                    </table></td>
                  </tr>
                </table></td>
              </tr>
           

