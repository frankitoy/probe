<?
//******************************************************************************************************
//   ATTENTION: THIS FILE HEADER MUST REMAIN INTACT. DO NOT DELETE OR MODIFY THIS FILE HEADER.
//
//   Name: uu_conlib.php
//   Revision: 1.2
//   Date: 2006/12/15
//   Link: http://uber-uploader.sourceforge.net  
//   Author: Peter Schmandra
//   Description: Gather stats on an existing upload
//
//   Licence:
//   The contents of this file are subject to the Mozilla Public
//   License Version 1.1 (the "License"); you may not use this file
//   except in compliance with the License. You may obtain a copy of
//   the License at http://www.mozilla.org/MPL/
// 
//   Software distributed under the License is distributed on an "AS
//   IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
//   implied. See the License for the specific language governing
//   rights and limitations under the License.
//
//***************************************************************************************************************

$path_to_upload_script      = '/cgi/uu_upload.pl'; 
$path_to_ini_status_script  = '/cgi/uu_ini_status.pl';
$default_config_file        = 'uu_default_config';
$disallow_extensions        = '/(\.sh)|(\.php)|(\.php3)|(\.php4)|(\.php5)|(\.py)|(\.shtml)|(\.phtml)|(\.cgi)|(\.pl)|(\.htaccess)|(\.htpasswd)$/i';
$allow_extensions           = '/(\.3gp)|(\.3gpp)|(\.mpg)|(\.mpeg)|(\.mp4)|(\.mpeg4)|(\.avi)|(\.wmv)|(\.flv)|(\.mov)|(\.asf)|(\.qt)$/i';
$multi_config_files         = 0;
$multi_upload_slots         = 0;
$max_upload_slots           = 20;
$check_file_name_format     = 1;
$check_disallow_extensions  = 0;
$check_allow_extensions     = 1;
$check_null_file_count      = 1;
$check_duplicate_file_count = 1;
$progress_bar_width         = 400;


/////////////////////////////////////////
// Output a message to screen and exit.
////////////////////////////////////////
function kak($msg){
	print "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n";
	print "  <html>\n";
	print "    <head>\n";
	print "      <title>UBER UPLOADER</title>\n";
	print "      <meta name=\"robots\" content=\"none\">\n"; 
	print "      <meta http-equiv=\"Pragma\" content=\"no-cache\">\n";
	print "      <meta http-equiv=\"CACHE-CONTROL\" content=\"no-cache\">\n";
	print "      <meta http-equiv=\"expires\" content=\"-1\">\n";
	print "    </head>\n";
	print "    <body style=\"background-color: #EEEEEE; color: #000000; font-family: arial, helvetica, sans_serif;\">\n";
	print "	     <br>\n";
	print "      <div align='center'>\n";
	print        $msg . "\n";
	print "      <br>\n";
	print "      <a href=\"http://sourceforge.net/projects/uber-uploader\"><font size='1'>Powered By Uber Uploader</font></a>\n";
	print "      </div>\n";
	print "    </body>\n";
	print "  </html>\n";
	exit;
}
?>
