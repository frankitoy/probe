<?php
/**************************************************************************************************
| Software Name        : ClipShare - Video Sharing Community Script
| Software Author      : Clip-Share.Com / ScriptXperts.Com
| Website              : http://www.clip-share.com
| E-mail               : office@clip-share.com
|**************************************************************************************************
| This source file is subject to the ClipShare End-User License Agreement, available online at:
| http://www.clip-share.com/video-sharing-script-eula.html
| By using this software, you acknowledge having read this Agreement and agree to be bound thereby.
|**************************************************************************************************
| Copyright (c) 2006-2007 Clip-Share.com. All rights reserved.
|**************************************************************************************************/

session_start();
include("include/config.php");
include("include/function.php");

if($config['enable_package']=="yes")
{
        if($_POST['submit'])
        {
                if($_POST['pack_id']=="")
                        $err = "To renew your account select one of the available packages";
                else
                        header("Location: pack_ops.php?pack=$_POST[pack_id]&uid=$_GET[uid]");
        }

        $sql = "select * from package where status = 'Active' and is_trial<>'yes' order by price desc";
        $rs = $conn->execute($sql);

        STemplate::assign('package', $rs->getrows());
}

if ( $_REQUEST[err] != "" )
{
        $err = $_REQUEST[err];
}
STemplate::assign('err',$err);
STemplate::assign('msg',$msg);
STemplate::assign('head_bottom',"homelinks.tpl");
STemplate::display('head1.tpl');
STemplate::display('err_msg.tpl');
STemplate::display('renew_account.tpl');
STemplate::display('footer.tpl');
?>
