<?
//******************************************************************************************************
//   ATTENTION: THIS FILE HEADER MUST REMAIN INTACT. DO NOT DELETE OR MODIFY THIS FILE HEADER.
//
//   Name: uu_finished.php
//   Revision: 2.9
//   Date: 2006/12/15 
//   Link: http://uber-uploader.sourceforge.net 
//   Author: Peter Schmandra  http://www.webdice.org
//   Description: Show successful file uploads.
//   Note: Pop ups may need to be enabled in the browser in order for this file to close the progress bar
//
//   Licence:
//   The contents of this file are subject to the Mozilla Public
//   License Version 1.1 (the "License"); you may not use this file
//   except in compliance with the License. You may obtain a copy of
//   the License at http://www.mozilla.org/MPL/
// 
//   Software distributed under the License is distributed on an "AS
//   IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
//   implied. See the License for the specific language governing
//   rights and limitations under the License.
//
//
//   NOTE: THIS FILE IS ONLY NECESSARY IF YOU ARE USING RE-DIRECT AFTER UPLOAD.
//
//***************************************************************************************************************

header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');

require "uu_conlib.php";

$uber_version = "4.2"; 
$this_version = "2.9";

/////////////////////////////////////////////////////////////////
// The following possible query string formats are assumed
//
// 1. ?upload_dir=tmp_sid&path_to_upload_dir
// 2. ?cmd=about
/////////////////////////////////////////////////////////////////
if(isset($_REQUEST['cmd']) && $_REQUEST['cmd'] == 'about'){ kak("<u><b>UBER UPLOADER FINISHED PAGE</b></u><br>UBER UPLOADER VERSION =  <b>" . $uber_version . "</b><br>UU_FINISHED = <b>" . $this_version . "<b><br>\n"); }
elseif(!isset($_REQUEST['tmp_sid']) || strlen($_REQUEST['tmp_sid']) != 32){ kak("<font color='red'>WARNING</font>: Invalid session-id.<br>\n"); }

// Hard-code the 'temp_dir' value here instead of passing it in the address bar.
$temp_dir = $_REQUEST['temp_dir'];

// Get all the posted values from the .param file
$_POST_DATA = getPostData($temp_dir, $_REQUEST['tmp_sid']);

////////////////////////////////////////////////////////////////////////////////////////////
// You can now access all the post values from the .param file. eg. $_POST_DATA['email']; //
////////////////////////////////////////////////////////////////////////////////////////////

// We must force flush incase email hangs
if($_POST_DATA['send_email_on_upload']){ ob_implicit_flush(1); }

$i = 0;
$file_list = "";
$email_file_list = "";

# Loop over the post data looking for files and create table elements
foreach($_POST_DATA as $key => $value){ 
	if(preg_match("/^upfile_/i", $key)){
		$uploaded_file_name = $value;
		$uploaded_file_path = $_POST_DATA['upload_dir'] . $uploaded_file_name;
		
		if(is_file($uploaded_file_path)){ 
			$file_size = filesize($uploaded_file_path); 
			$file_size = formatBytes($file_size);
    		
			if($i%=2){ $bg_col = "cccccc"; }
			else{ $bg_col = "dddddd"; }
			
			if($_POST_DATA['link_to_upload']){
				$path_to_upload = $_POST_DATA['path_to_upload'];
				$file_list .= "<tr><td align=\"center\" bgcolor=\"$bg_col\"><a href=\"$path_to_upload$uploaded_file_name\" target=\"_blank\">$uploaded_file_name</a></td><td align=\"center\" bgcolor=\"$bg_col\">$file_size</td></tr>\n"; 
			}
			else{ $file_list .= "<tr><td align=\"center\" bgcolor=\"$bg_col\">$uploaded_file_name</td><td align=\"center\" bgcolor=\"$bg_col\">$file_size</td></tr>\n"; }
			
			if($_POST_DATA['link_to_upload_in_email']){ $email_file_list .= "File Name:" . $_POST_DATA['path_to_upload'] . $uploaded_file_name . "     File Size: " . $file_size. "\n"; }
			else{ $email_file_list .= "File Name: $uploaded_file_name     File Size: $file_size\n"; }
			
			$i++;
		}
		else{
			if($i%=2){ $bg_col = "cccccc"; }
			else{ $bg_col = "dddddd"; }
			
			$email_file_list .= "File Name: $uploaded_file_name     File Size: Failed To Upload !\n";
    			
			$file_list .= "<tr><td align=\"center\" bgcolor=\"$bg_col\">&nbsp;$uploaded_file_name&nbsp;</td><td align=\"center\" bgcolor=\"$bg_col\"><font color=\"red\">Failed To Upload</font></td></tr>\n"; 
			$i++;
		}
		clearstatcache();
	} 
}

// Send upload results if email on upload is enabled
if($_POST_DATA['send_email_on_upload']){ email_upload_results($_POST_DATA['to_email_address'], $_POST_DATA['from_email_address'], $_POST_DATA['email_subject'], $email_file_list, $_REQUEST['tmp_sid'], $_POST_DATA['start_time'], $_POST_DATA['end_time'], $_POST_DATA['html_email_support']); }

/////////////////////////////////////////////////////////
//  Get the post data from the param file (tmp_sid.param)
/////////////////////////////////////////////////////////
function getPostData($up_dir, $tmp_sid){
	$param_array = array();
	$buffer = "";
	$key = "";
	$value = "";
	$paramFileName = $up_dir . $tmp_sid . ".params";
	$fh = fopen($paramFileName, 'r') or kak("<font color='red'>ERROR</font>: Can't open $paramFileName");
	
	while(!feof($fh)){
		$buffer = fgets($fh, 4096);
		list($key, $value) = explode('=', trim($buffer));
		$value = str_replace("~EQLS~", "=", $value);
		$value = str_replace("~NWLN~", "\r\n", $value);
		
		if(isset($key) && isset($value) && strlen($key) > 0 && strlen($value) > 0){
			if(preg_match('/(.*)\[(.*)\]/i', $key, $match)){ $param_array[$match[1]][$match[2]] = $value; } 
			else{ $param_array[$key] = $value; }
		}
	}

	fclose($fh);
	
	if(isset($param_array['delete_param_file']) && $param_array['delete_param_file'] == 1){
		for($i = 0; $i < 5; $i++){
			if(unlink($paramFileName)){ break; }
			else{ sleep(1); }
		}
	}
			
	return $param_array;
}

//////////////////////////////////////////////////
//  formatBytes($file_size) mixed file sizes
//  formatBytes($file_size, 0) KB file sizes
//  formatBytes($file_size, 1) MB file sizes etc
//////////////////////////////////////////////////
function formatBytes($bytes, $format=99){
	$byte_size = 1024;
	$byte_type = array(" KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
	
	$bytes /= $byte_size;
	$i = 0;
	
	if($format == 99 || $format > 7){
		while($bytes > $byte_size){
			$bytes /= $byte_size;
			$i++;
		}
	}
	else{
		while($i < $format){
			$bytes /= $byte_size;
			$i++;
		}
	}
	
	$bytes = sprintf("%1.2f", $bytes);
	$bytes .= $byte_type[$i];
	
	return $bytes;		
}



//////////////////////////////////////////
// Send an email with the upload results.
////////////////////////////////////////// 
function email_upload_results($email_to, $email_from, $email_subject, $upload_result, $tmp_sid, $start_time, $end_time, $html_email){
	if($html_email){
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1; format=flowed' . "\r\n";
	}
	
	$headers .= "To: " . $email_to . "\r\n";
	$headers .= "From: " . $email_from . "\r\n";
	
	$message = "\nStart Upload (epoch): " . $start_time . "\n";
	$message .= "End Upload (epoch): " . $end_time . "\n";
	$message .= "Start Upload: ". date("M j, Y, g:i:s", $start_time) . "\n";
	$message .= "End Upload: ". date("M j, Y, g:i:s", $end_time) . "\n";
	$message .= "SID: ". $tmp_sid . "\n";
	$message .= "Remote IP: " . $_SERVER['REMOTE_ADDR']  . "\n";
	$message .= "Browser: " . $_SERVER['HTTP_USER_AGENT'] . "\n\n";
	$message .= $upload_result;
	
	mail($email_to, $email_subject, $message, $headers);
} 

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>UBER UPLOADER</title>
    <meta name="robots" content="none">
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" > 
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
  </head>
  <body style="background-color: #EEEEEE; color: #000000; font-family: arial, helvetica, sans_serif;">
<br>
<div align="center">
  <br>
  <table cellpadding="1" cellspacing="1" width="70%" >
    <tr>
      <td align="center" bgcolor="bbbbbb">&nbsp;&nbsp;<b>UPLOADED FILE NAME</b>&nbsp;&nbsp;</td><td align="center" bgcolor="bbbbbb">&nbsp;&nbsp;<b>UPLOADED FILE SIZE</b>&nbsp;&nbsp;</td>
    </tr>
    <? print $file_list; ?>
  </table>
  <br>
  <a href="http://sourceforge.net/projects/uber-uploader"><font size="1">Powered By Uber Uploader</font></a>
  <br>
  <br>
  <input type="button" value="Go Back" onClick="javascript:top.location.href='uu_file_upload.php'">
</div>
</body>
</html>




