<? 
//******************************************************************************************************
//   ATTENTION: THIS FILE HEADER MUST REMAIN INTACT. DO NOT DELETE OR MODIFY THIS FILE HEADER.
//
//   Name: uu_file_upload.php
//   Revision: 1.2
//   Date: 2006/12/15
//   Link: http://uber-uploader.sourceforge.net 
//   Author: Peter Schmandra  http://www.webdice.org
//   Description: Upload files and show progress in popup.
//   Note: The popup is called on the submit and it submits the upload. This method defeats pop up blockers
//
//   Licence:
//   The contents of this file are subject to the Mozilla Public
//   License Version 1.1 (the "License"); you may not use this file
//   except in compliance with the License. You may obtain a copy of
//   the License at http://www.mozilla.org/MPL/
// 
//   Software distributed under the License is distributed on an "AS
//   IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
//   implied. See the License for the specific language governing
//   rights and limitations under the License.
//
//***************************************************************************************************************

header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');

include "uu_conlib.php";

$uber_version = "4.2"; 
$this_version = "1.2";

if(isset($_REQUEST['cmd']) && $_REQUEST['cmd'] == 'about'){ kak("<u><b>UBER UPLOADER FILE UPLOAD</b></u><br>UBER UPLOADER VERSION =  <b>" . $uber_version . "</b><br>UU_FILE_UPLOAD = <b>" . $this_version . "<b><br>\n"); }

$tmp_sid = md5(uniqid(mt_rand(), true));
$config_file = $default_config_file;
$path_to_upload_script .= '?tmp_sid=' . $tmp_sid;
$path_to_ini_status_script .= '?tmp_sid=' . $tmp_sid; 

if($multi_config_files){ 
	$path_to_upload_script .= "&config_file=$config_file";
	$path_to_ini_status_script .= "&config_file=$config_file"; 
}                                                                 

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>Uber-Uploader - Free File Upload Progress Bar</title>
  <meta name="robots" content="index,nofollow"> 
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" > 
  <meta http-equiv="Pragma" content="no-cache">
  <meta http-equiv="cache-control" content="no-cache">
  <meta http-equiv="expires" content="-1">
  <!-- Please do not remove this tag: Uber-Uploader Ver 4.2 http://uber-uploader.sourceforge.net -->
<style>
  .info {font:18px Arial;}
  .data {background-color:#b3b3b3; border:1px solid #898989; width:40%;}
  .data tr td {background-color:#dddddd; font:13px Arial; width:35%;}
  .bar1 {background-color:#b3b3b3; position:relative; text-align:left; height:20px; width:<? print $progress_bar_width; ?>px; border:1px solid #505050;}
  .bar2 {background-color:#000099; position:relative; text-align:left; height:20px; width:0%;}
</style>
</head>

<script language="javascript" type="text/javascript">
  var tmp_sid = "<? print $tmp_sid; ?>"; 
  var disallow_extensions = <? print $disallow_extensions; ?>;
  var allow_extensions = <? print $allow_extensions; ?>;
  var path_to_ini_status_script = "<? print $path_to_ini_status_script; ?>";
  var check_file_name_format = <? print $check_file_name_format; ?>;
  var check_disallow_extensions = <? print $check_disallow_extensions; ?>;
  var check_allow_extensions = <? print $check_allow_extensions; ?>;
  var check_null_file_count = <? print $check_null_file_count; ?>;
  var check_duplicate_file_count = <? print $check_duplicate_file_count; ?>;
  var max_upload_slots = <? print $max_upload_slots; ?>;
  var progress_bar_width = <? print $progress_bar_width; ?>;
                         
</script>
<script language="javascript" type="text/javascript" src="uu_file_upload.js"></script>

<body style="background-color: #EEEEEE; color: #000000; font-family: arial, helvetica, sans_serif;" onLoad="iniFilePage()">
<? print "<!-- TMP_SID: $tmp_sid -->\n"; ?>
  <div align="center">
    <br>
    <!-- Start Progress Bar -->
    <div class="info" id="progress_info"></div>
    <div id="progress_bar" style="display:none">
      <div class="bar1" id="upload_status_wrap" align="center">
        <div class="bar2" id="upload_status"></div>
      </div>
      <br>
      <table class="data" cellpadding='3' cellspacing='1'>
        <tr>
          <td align="left"><b>Percent Complete:</b></td>
          <td align="center"><span id="percent">0%</span></td>
        </tr>
        <tr>
          <td><b>Files Uploaded:</b></td>
          <td align="center"><span id="uploaded_files">0</span> of <span id="total_uploads"></span></td>
        </tr>
        <tr>
          <td align="left"><b>Current Position:</b></td>
          <td align="center"><span id="current">0</span> / <span id="total_kbytes"></span> KBytes</td>
        </tr>
        <tr>
          <td align="left"><b>Elapsed time:</b></td>
          <td align="center"><span id="time">0</span></td>
        </tr>
        <tr>
          <td align="left"><b>Est Time Left:</b></td>
          <td align="center"><span id="remain">0</span></td>
        </tr>
        <tr>
          <td align="left"><b>Est Speed:</b></td>
          <td align="center"><span id="speed">0</span> KB/s.</td>
        </tr>
      </table>
    </div>
    <!-- End Progress Bar -->
    <br>  
    <form name="form_upload" method="post" enctype="multipart/form-data"  action="<? print $path_to_upload_script; ?>" style="margin: 0px; padding: 0px;">
      <input type="hidden" name="upload_range" value="1">
      <noscript><input type="hidden" name="no_script" value="1"></noscript>
      <!-- Include extra values you want passed to the upload script here. -->
      <!-- eg. <input type="text" name="employee_num" value="5"> -->
      <!-- Access the value in the cgi with $query->param('employee_num'); -->
      <!-- Access the value in the php finished page with $_POST_DATA['employee_num']; -->
      <!-- DO NOT USE "upfile_" for any of your values. --> 
      <div id="upload_slots">
        <input type="file" name="upfile_0" size="90" <? if($multi_upload_slots){ ?>onChange="addUploadSlot(1)"<? } ?> value="">
      </div> 
      <noscript><br><input type="reset" name="no_script_reset" value="Reset">&nbsp;&nbsp;&nbsp;<input type="submit" name="no_script_submit" value="Upload"></noscript> 
    </form>
    <br>
    <script language="javascript" type="text/javascript">
    <!--  
        document.writeln('<input type="button" name="reset_button" value="Reset" onClick="resetForm();">&nbsp;&nbsp;&nbsp;<input type="button" id="upload_button" name="upload_button" value="Upload" onClick="uploadFiles();">');
    //-->
    </script>
  </div>
   <br>
   
</body>
</html>
