<?php
/**************************************************************************************************
| Software Name        : ClipShare - Video Sharing Community Script
| Software Author      : Clip-Share.Com / ScriptXperts.Com
| Website              : http://www.clip-share.com
| E-mail               : office@clip-share.com
|**************************************************************************************************
| This source file is subject to the ClipShare End-User License Agreement, available online at:
| http://www.clip-share.com/video-sharing-script-eula.html
| By using this software, you acknowledge having read this Agreement and agree to be bound thereby.
|**************************************************************************************************
| Copyright (c) 2006-2007 Clip-Share.com. All rights reserved.
|**************************************************************************************************/

session_start();
include("include/config.php");
include("include/function.php");


        $UID = base64_decode($_GET['uid']);

        /* Get User Information */
        $sql = "select * from signup where UID=$UID";
        $rs = $conn->execute($sql);
        $email = $rs->fields['email'];
        
        STemplate::assign('fname', $rs->fields['fname']);
        STemplate::assign('lname', $rs->fields['lname']);
        STemplate::assign('city', $rs->fields['city']);
        STemplate::assign('country',country_box($rs->fields['country']));
        
        /* Get Package Information*/
        $sql = "select * from package where pack_id=$_GET[pack]";
        $rs = $conn->execute($sql);
        $package = $rs->getrows();
        $totalprice = $_POST['period']*$package[0]['price'];

        STemplate::assign('package', $package[0]);
        STemplate::assign('totalprice',$totalprice);

        if($_POST['submit'])
        {
                if($_POST['method'] == "Paypal")
                {

                                $conn->execute("update signup set
                                                fname = '$_POST[fname]',
                                                lname = '$_POST[lname]',
                                                city = '$_POST[city]',
                                                country = '$_POST[country]'
                                        where UID = $UID
                                        ");


                                $s_period = $_POST[period]." ".$package[0][period];
                                $theprice = $totalprice;
                                $invoice = $UID."|".$_GET['pack']."|".$s_period."|".$totalprice;

                                /* Paypal Payment */
                                $theamount=$theprice;
                                $uniqueid=$invoice;
                                $business =urlencode($config[paypal_receiver_email]);
                                $item_name = urlencode("For Package : ".$package[0]['pack_name']);
                                $return = "$config[BASE_URL]/pmt_success.php?pack=$_GET[pack]&is_success=1&period=$s_period";
                                $cancel = "$config[BASE_URL]/pmt_success.php?pack=$_GET[pack]&is_success=0";
                                $notify = "$config[BASE_URL]/payment/notify_paypal.php";

                                $return = urlencode($return);
                                $cancel = urlencode($cancel);
                                $notify = urlencode($notify);
                                $first_name = urlencode($_POST['fname']);
                                $last_name = urlencode($_POST['lname']);
                                $city = urlencode($_POST['city']);
                                
                                if($config['enable_test_payment']=="yes")
                                        $url = "www.sandbox.paypal.com";
                                else
                                        $url = "www.paypal.com";

                                $paypal_link="https://$url/cgi-bin/webscr/?cmd=_xclick"
                                                        ."&business=$business"
                                                         ."&item_number=1&item_name=$item_name"
                                                         ."&amount=$theprice&on0=0&custom=$uniqueid"
                                                         ."&currency_code=USD"
                                                         ."&return=$return"
                                                         ."&cancel_return=$cancel"
                                                         ."&notify_url=$notify"
                                                         ."&first_name=$first_name"
                                                         ."&last_name=$last_name"
                                                         ."&city=$city";
                                redirect($paypal_link);

                                //$success = true;

                }


        }
        
        

        
        

        STemplate::assign('err',$err);
        STemplate::assign('head_bottom',"blank.tpl");
        STemplate::display('head1.tpl');
        STemplate::display('err_msg.tpl');
        STemplate::display('payment.tpl');
        STemplate::display('footer.tpl');
?>
