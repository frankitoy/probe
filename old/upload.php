<?php
/**************************************************************************************************
| Software Name        : ClipShare - Video Sharing Community Script
| Software Author      : Clip-Share.Com / ScriptXperts.Com
| Website              : http://www.clip-share.com
| E-mail               : office@clip-share.com
|**************************************************************************************************
| This source file is subject to the ClipShare End-User License Agreement, available online at:
| http://www.clip-share.com/video-sharing-script-eula.html
| By using this software, you acknowledge having read this Agreement and agree to be bound thereby.
|**************************************************************************************************
| Copyright (c) 2006-2007 Clip-Share.com. All rights reserved.
|**************************************************************************************************/

    include("include/config.php");
    include("include/function.php");
    chk_member_login();
    isMailVerified();
    if($config['enable_package']=="yes")
    {
        check_subscriber(100);
    }

    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header('Cache-Control: no-store, no-cache, must-revalidate');
    header('Cache-Control: post-check=0, pre-check=0', FALSE);
    header('Pragma: no-cache');

    include "uu_conlib.php";
    $tmp_sid = md5(uniqid(mt_rand(), true));
    $config_file = $default_config_file;                                                                                                   
    $path_to_upload_script .= '?tmp_sid=' . $tmp_sid;
    $path_to_ini_status_script .= '?tmp_sid=' . $tmp_sid;

    function getPostData($up_dir, $tmp_sid){
	$param_array = array();
	$buffer = "";
	$key = "";
	$value = "";
	$paramFileName = $up_dir . $tmp_sid . ".params";
	$fh = fopen($paramFileName, 'r') or kak("<font color='red'>ERROR</font>: Can't open $paramFileName");
	
	while(!feof($fh)){
		$buffer = fgets($fh, 4096);
		list($key, $value) = explode('=', trim($buffer));
		$value = str_replace("~EQLS~", "=", $value);
		$value = str_replace("~NWLN~", "\r\n", $value);
		
		if(isset($key) && isset($value) && strlen($key) > 0 && strlen($value) > 0){
			if(preg_match('/(.*)\[(.*)\]/i', $key, $match)){ $param_array[$match[1]][$match[2]] = $value; } 
			else{ $param_array[$key] = $value; }
		}
	}

	fclose($fh);
	
	if(isset($param_array['delete_param_file']) && $param_array['delete_param_file'] == 1){
		for($i = 0; $i < 5; $i++){
			if(unlink($paramFileName)){ break; }
			else{ sleep(1); }
		}
	}
			
	return $param_array;
    }

    STemplate::assign('tmp_sid', $tmp_sid);
    STemplate::assign('disallow_extensions', $disallow_extensions);
    STemplate::assign('allow_extensions', $allow_extensions);
    STemplate::assign('path_to_ini_status_script', $path_to_ini_status_script);
    STemplate::assign('check_file_name_format', $check_file_name_format);
    STemplate::assign('check_disallow_extensions', $check_disallow_extensions);
    STemplate::assign('check_allow_extensions', $check_allow_extensions);
    STemplate::assign('check_null_file_count', $check_null_file_count);
    STemplate::assign('check_duplicate_file_count', $check_duplicate_file_count);
    STemplate::assign('max_upload_slots', $max_upload_slots);
    STemplate::assign('progress_bar_width', $progress_bar_width);
    STemplate::assign('path_to_upload_script', $path_to_upload_script);
    STemplate::assign('multi_upload_slots', $multi_upload_slots);

if($_REQUEST[action_upload]!="")
{

  if(strlen($_REQUEST[field_myvideo_title]) < 3) 
    $err = "Upload: Please provide a video title with minimum 3 characters."; 

  elseif(preg_match("/[^a-zA-Z0-9 ������!?\_\-\.]/", $_REQUEST[field_myvideo_title])) 
    $err = "Upload: Unallowed characters in video title."; 

  elseif(strlen($_REQUEST[field_myvideo_descr]) < 3) 
    $err = "Upload: Please provide a description with min of 3 characters."; 

  elseif(preg_match("/[^a-zA-Z0-9 ������\!\?\_\-\.]/", $_REQUEST[field_myvideo_descr])) 
    $err = "Upload: Unallowed characters in description."; 

  elseif(strlen($_REQUEST[field_myvideo_keywords]) < 1) 
    $err = "Upload: Please provide tag(s)."; 

  elseif(preg_match("/[^a-zA-Z0-9 ������\-]/", $_REQUEST[field_myvideo_keywords])) 
    $err = "Upload: Tags should be seperated only by spaces"; 

  elseif(count($_REQUEST[chlist])<1 || count($_REQUEST[chlist])>3) 
    $err="Upload: Please check (1 to 3) channel(s)."; 
		
        if($err==""){
			$page = "second";
			STemplate::assign('secondpage',"second");
			$listch=implode("|",$_REQUEST[chlist]);
			STemplate::assign('listch',$listch);
			$var = "<script language=javascript>
					var field_myvideo_title = $_REQUEST[field_myvideo_title];
					var field_myvideo_descr = $_REQUEST[field_myvideo_descr];
					var field_myvideo_keywords = $_REQUEST[field_myvideo_keywords];
					var listch = $listch;
					</script>";
			STemplate::assign("var", $var);
        }
}

if(isset($_GET['rnd_id']))
{
    STemplate::assign('upload_page', 'upload');
        
    $temp_dir = $_REQUEST['temp_dir'];
    $_POST_DATA = getPostData($temp_dir, $_REQUEST['tmp_sid']);
    
    $upload_dir = $_POST_DATA['upload_dir'];
    $upload_file = $_POST_DATA['upfile_0'];
    $upload_file_path = $upload_dir . $upload_file;
    if(!is_file($upload_file_path) || filesize($upload_file_path) < 0)
    {
	$err = 'Failed to upload selected file!';
    }
    
    $upload_file_size = filesize($uploaded_file_path);
    if($err == "")
    {
	$pos = strrpos($upload_file,".");
	$ph = strtolower(substr($upload_file,$pos+1,strlen($upload_file)-$pos));
	$space = round($upload_file_size/(1024*1024));
	if($config['enable_package']=="yes")
	{
	    check_subscriber($space);
	}
	
	//this should never happen
	if(($ph!="3gp" && $ph!="mp4" && $ph!="mov" && $ph!="asf" && $ph!="flv" && $ph!="mpg" && $ph!="avi" && $ph!="mpeg" && $ph!="wmv" && $ph!="rm" && $ph!="dat") || $space>$config[max_video_size])
	    $err = 'Invalid video format or invalid video size!';	
    }
    
    if($err == "")
    {

			// ----- Approve Video -----
			if($config['approve'] == 1) {
				$active="active='0'";
				} else {
				$active="active='1'";
				}
			// -------------------------

  $v_title = htmlentities(strip_tags($_POST_DATA[field_myvideo_title])); 
  $v_descr = htmlentities(strip_tags($_POST_DATA[field_myvideo_descr])); 

  $sql="insert into video set 
    UID=$_SESSION[UID], 
    title='$v_title', 
    description='$v_descr', 
    keyword='$_POST_DATA[field_myvideo_keywords]', 
    channel='0|$_POST_DATA[listch]|0', 
    space = '$space', 
    addtime='".time()."', 
    adddate='".date("Y-m-d")."', 
    vkey='".mt_rand()."', 
    type='$_POST_DATA[field_privacy]',
	$active, 
    filehome='$_POST_DATA[p]'";

	$conn->execute($sql);
    	$vid=mysql_insert_id();
        $vdoname=$vid.".".$ph;
	$ff = $config['vdodir'].'/'.$vdoname;
	
	//rename uploaded file
	if(rename($upload_file_path,$ff))
	{
	    //create background conversion process

if($config[vresize] == 1) {$encodecommand="$config[mencoder] $config[vdodir]/$vdoname -o $config[flvdodir]/".$vid.".flv -of lavf -oac mp3lame -lameopts abr:br=56 -ovc lavc -lavcopts vcodec=flv:vbitrate=$config[vbitrate]:mbd=2:mv0:trell:v4mv:cbp:last_pred=3 -lavfopts i_certify_that_my_video_stream_does_not_use_b_frames -vop scale=$config[vresize_x]:$config[vresize_y] -srate $config[sbitrate]";}
else {$encodecommand="$config[mencoder] $config[vdodir]/$vdoname -o $config[flvdodir]/".$vid.".flv -of lavf -oac mp3lame -lameopts abr:br=56 -ovc lavc -lavcopts vcodec=flv:vbitrate=$config[vbitrate]:mbd=2:mv0:trell:v4mv:cbp:last_pred=3 -lavfopts i_certify_that_my_video_stream_does_not_use_b_frames -srate $config[sbitrate]";}

		exec("$config[phppath] $config[BASE_DIR]/convert.php $vdoname $vid $ff> /dev/null &"); 
	    
		//create temporary (video is converting) thumbs	
		video_to_frame2($ff, $vid);
			
		//get duration
		exec("$config[mplayer] -vo null -ao null -frames 0 -identify $config[vdodir]/$vdoname", $p);
    		while(list($k,$v)=each($p))
    		{
        	    if($length=strstr($v,'ID_LENGTH='))
        	    break;
    		}
    		$lx = explode("=",$length);
    		$duration = $lx[1];

	} else {
	    $err = 'Failed to rename uploaded file!';
	}
    }
    
    if($err == "")
    {
	$key=substr(md5($vid),11,20);
	send_subscribed_mail($_SESSION[UID],'$key',$_SESSION[EMAIL]);

        $sql="update video set
    		vdoname='$vdoname',
                flvdoname='".$vid.".flv',
                duration='$duration',
                vkey='$key' WHERE VID=$vid";
        $conn->execute($sql);
       
 
        if($config['enable_package']=="yes")
        {
    	    $sql = "update subscriber set used_space=used_space+$space, used_bw=used_bw+$space, total_video=total_video+1 where UID=$_SESSION[UID]";
            $conn->execute($sql);
        }
        
        header("Location:$config[baseurl]/upload_success.php?viewkey=$key&upload=yes");
    }
}

STemplate::assign('err',$err);
STemplate::assign('msg',$msg);
STemplate::assign('upload_page', 'upload');
STemplate::assign('menu_active', 'upload'); 
STemplate::assign('head_bottom',"blank.tpl");
STemplate::display('head1.tpl');
STemplate::display('err_msg.tpl');
STemplate::display('upload.tpl');
STemplate::display('footer.tpl');
?>
