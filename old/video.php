<?php
/**************************************************************************************************
| Software Name        : ClipShare - Video Sharing Community Script
| Software Author      : Clip-Share.Com / ScriptXperts.Com
| Website              : http://www.clip-share.com
| E-mail               : office@clip-share.com
|**************************************************************************************************
| This source file is subject to the ClipShare End-User License Agreement, available online at:
| http://www.clip-share.com/video-sharing-script-eula.html
| By using this software, you acknowledge having read this Agreement and agree to be bound thereby.
|**************************************************************************************************
| Copyright (c) 2006-2007 Clip-Share.com. All rights reserved.
|**************************************************************************************************/

session_start();
include("include/config.php");
include("include/function.php");
if ($config['approve'] == 1) {$active = "and active = '1'";}

//PAGING
if($_REQUEST[page]=="")
$page = 1;
else
$page = $_REQUEST[page];

if($_REQUEST[category]!="rd" && $_REQUEST[category]!="mv" && $_REQUEST[category]!="md" && $_REQUEST[category]!="tf" && $_REQUEST[category]!="tr" && $_REQUEST[category]!="rf")
        $category="mr";
else
        $category = $_REQUEST[category];

if($category=="mr")$sql = "SELECT count(*) as total from video where type='public' $active order by addtime desc limit $config[total_per_ini]";
elseif($category=="mv")$sql = "SELECT count(*) as total from video where type='public' $active order by viewnumber desc limit $config[total_per_ini]";
elseif($category=="md")$sql = "SELECT count(*) as total from video where type='public' $active order by com_num desc limit $config[total_per_ini]";
elseif($category=="tf")$sql = "SELECT count(*) as total from video where type='public' $active order by fav_num desc limit $config[total_per_ini]";
elseif($category=="tr")$sql = "SELECT count(*) as total from video where type='public' $active order by ratedby*rate desc limit $config[total_per_ini]";
elseif($category=="rf")$sql = "SELECT count(*) as total from video where type='public' $active and featured='yes' order by addtime desc limit $config[total_per_ini]";
elseif($category=="rd")$sql = "SELECT count(*) as total from video where type='public' $active order by rand() limit $config[total_per_ini]";

$ars = $conn->Execute($sql);
if($ars->fields['total']<=$config[total_per_ini])$total = $ars->fields['total'];
else $total = $config[total_per_ini];
$tpage = ceil($total/$config[items_per_page]);
if($tpage==0) $spage=$tpage+1;
else $spage = $tpage;
$startfrom = ($page-1)*$config[items_per_page];
//generate next and previous
$next="";
$prev="";
$next_page=$page+1;
$prev_page=$page-1;
if($page>1 && $page<$spage)
{
        $next="<b><a href='video.php?page=$next_page'>Next</a><b>";
        $prev="<b><a href='video.php?page=$prev_page'>Previous</a><b>";
}
else if($page<$spage)
{
        $next="<b><a href='images.php?page=$next_page'>Next</a><b>";
}
else if($page>1 && $page=$spage)
{
        $prev="<b><a href='images.php?page=$prev_page'>Previous</a><b>";
}

if($category=="mr")$sql="SELECT * from video where type='public' $active order by addtime desc limit $startfrom, $config[items_per_page]";
elseif($category=="mv")$sql="SELECT * from video where type='public' $active order by viewnumber desc limit $startfrom, $config[items_per_page]";
elseif($category=="md")$sql="SELECT * from video where type='public' $active order by com_num desc limit $startfrom, $config[items_per_page]";
elseif($category=="tf")$sql="SELECT * from video where type='public' $active order by fav_num desc limit $startfrom, $config[items_per_page]";
elseif($category=="tr")$sql="SELECT * from video where type='public' $active order by (ratedby*rate) desc limit $startfrom, $config[items_per_page]";
elseif($category=="rf")$sql="SELECT * from video where type='public' $active and featured='yes' order by addtime desc limit $startfrom, $config[items_per_page]";
elseif($category=="rd")$sql="SELECT * from video where type='public' $active order by rand() limit $startfrom, $config[items_per_page]";

$rs = $conn->Execute($sql);
$users = $rs->getrows();
$start_num=$startfrom+1;
$end_num=$startfrom+$rs->recordcount();

$page_link="";
for($k=1;$k<=$tpage;$k++){ 
        
        $page_link.="<a href='video.php?page=$k&viewtype=$_REQUEST[viewtype]&category=$category'>$k</a>&nbsp;&nbsp;";
}

//END PAGING

STemplate::assign('err',$err);
STemplate::assign('msg',$msg);
STemplate::assign('page',$page);
STemplate::assign('start_num',$start_num);
STemplate::assign('end_num',$end_num);
STemplate::assign('placement',$placement);
STemplate::assign('page_link',$page_link);
STemplate::assign('total',$total);
STemplate::assign('answers',$users);
STemplate::assign('next',$next);
STemplate::assign('prev',$prev);
STemplate::assign('head_bottom',"videolinks.tpl");
STemplate::display('head1.tpl');
STemplate::display('err_msg.tpl');
STemplate::display('search.tpl');
STemplate::display('video.tpl');
STemplate::display('footer.tpl');
?>
