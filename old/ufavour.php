<?php

/**************************************************************************************************

| Software Name        : ClipShare - Video Sharing Community Script

| Software Author      : Clip-Share.Com / ScriptXperts.Com

| Website              : http://www.clip-share.com

| E-mail               : office@clip-share.com

|**************************************************************************************************

| This source file is subject to the ClipShare End-User License Agreement, available online at:

| http://www.clip-share.com/video-sharing-script-eula.html

| By using this software, you acknowledge having read this Agreement and agree to be bound thereby.

|**************************************************************************************************

| Copyright (c) 2006-2007 Clip-Share.com. All rights reserved.

|**************************************************************************************************/



session_start();

include("include/config.php");

include("include/function.php");

if ($config['approve'] == 1) {$active = "and active = '1'";}


//PAGING STARTS

if($page=="")

$page = 1;

$sql = "SELECT count(*) as total from video as v, favourite as f WHERE f.UID=$_REQUEST[UID] and f.VID=v.VID $active limit $config[total_per_ini]";

$ars = $conn->Execute($sql);

if($ars->fields['total']<=$config[total_per_ini])$total = $ars->fields['total'];

else $total = $config[total_per_ini];

$tpage = ceil($total/$config[items_per_page]);

if($tpage==0) $spage=$tpage+1;

else $spage = $tpage;

$startfrom = ($page-1)*$config[items_per_page];



$sql="select * from video as v, favourite as f WHERE f.UID=$_REQUEST[UID] and f.VID=v.VID $active limit $startfrom, $config[items_per_page]";

$rs=$conn->execute($sql);

if($rs->recordcount()>0)$vdo = $rs->getrows();

$start_num=$startfrom+1;

$end_num=$startfrom+$rs->recordcount();



$page_link="";

for($k=1;$k<=$tpage;$k++) $page_link.="<a href='ufavour.php?UID=$_REQUEST[UID]&page=$k&type=$_REQUEST[type]'>$k</a>&nbsp;&nbsp;";

//END PAGING





STemplate::assign('err',$err);

STemplate::assign('msg',$msg);

STemplate::assign('page',$page);

STemplate::assign('start_num',$start_num);

STemplate::assign('end_num',$end_num);

STemplate::assign('page_link',$page_link);

STemplate::assign('total',$total);

STemplate::assign('answers',$vdo);

$tags=group_tags($sql);

STemplate::assign('tags',$tags);

STemplate::assign('head_bottom',"blank.tpl");

STemplate::assign('head_bottom_add',"viewuserlinks.tpl");

STemplate::display('head1.tpl');

STemplate::display('err_msg.tpl');

STemplate::display('ufavour.tpl');

STemplate::display('footer.tpl');

?>

