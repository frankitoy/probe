<?php
/**************************************************************************************************
| Software Name        : ClipShare - Video Sharing Community Script
| Software Author      : Clip-Share.Com / ScriptXperts.Com
| Website              : http://www.clip-share.com
| E-mail               : office@clip-share.com
|**************************************************************************************************
| This source file is subject to the ClipShare End-User License Agreement, available online at:
| http://www.clip-share.com/video-sharing-script-eula.html
| By using this software, you acknowledge having read this Agreement and agree to be bound thereby.
|**************************************************************************************************
| Copyright (c) 2006-2007 Clip-Share.com. All rights reserved.
|**************************************************************************************************/

session_start();
include("include/config.php");

$_SESSION[UID]="";session_unregister("UID");
$_SESSION[EMAIL]="";session_unregister("EMAIL");
$_SESSION[USERNAME]="";session_unregister("USERNAME");

//echo "<table align=center><tr><td><br><br><b><center>You Have been Successfully logged out.<br>You will be redirected to Main site in few seconds.</center></b></td></tr></table>";
//echo "<Meta HTTP-EQUIV=refresh content=\"2; url=$config[baseurl]/index.php\">";
header("location: $config[baseurl]/index.php");
?>
