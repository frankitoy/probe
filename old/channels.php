<?php
/**************************************************************************************************
| Software Name        : ClipShare - Video Sharing Community Script
| Software Author      : Clip-Share.Com / ScriptXperts.Com
| Website              : http://www.clip-share.com
| E-mail               : office@clip-share.com
|**************************************************************************************************
| This source file is subject to the ClipShare End-User License Agreement, available online at:
| http://www.clip-share.com/video-sharing-script-eula.html
| By using this software, you acknowledge having read this Agreement and agree to be bound thereby.
|**************************************************************************************************
| Copyright (c) 2006-2007 Clip-Share.com. All rights reserved.
|**************************************************************************************************/

session_start();
include("include/config.php");
include("include/function.php");


//PAGING
$sql="SELECT * from channel";
$rs = $conn->Execute($sql);
$users = $rs->getrows();
//END PAGING

STemplate::assign('err',$err);

STemplate::assign('msg',$msg);
STemplate::assign('answers',$users);
STemplate::display('head1.tpl');
STemplate::display('search.tpl');


if($flag=="audio_channels.tpl")
{
        STemplate::display($flag);
}
else
{
        STemplate::display('channels.tpl');
}

STemplate::display('footer.tpl');
?>
